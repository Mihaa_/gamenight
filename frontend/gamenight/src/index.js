import React from 'react';
import ReactDOM from 'react-dom';
import './style/index.css';
import AppRouter from './routes/AppRouter';

const jsx = (
    
      <AppRouter />
  );

ReactDOM.render(jsx, document.getElementById('root'));

