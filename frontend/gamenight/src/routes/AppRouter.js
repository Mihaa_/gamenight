import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import SignInForm from '../components/SignInForm';
import SignUpForm from '../components/SignUpForm';
import NotFoundPage from '../components/NotFoundPage';
import GamePage from '../components/GamePage';
import AddGameForm from '../components/AddGameForm';
import UsersListPage from '../components/UsersListPage';
import StvoriDogadajForm from '../components/StvoriDogadajForm';
import MainBody from '../components/MainBody';
import UserProfile from '../components/UserProfile';
import EventVotingForm from '../components/EventVotingForm';
import DogadajList from '../components/DogadajList';
import AttendantsForm from '../components/AttendantsForm';


const AppRouter = () => (
    <BrowserRouter>
      <div>
        <Switch>
          <Route path="/" component={MainBody} exact={true} />
          <Route path="/welcome" component={DogadajList} />
          <Route path="/signin" component={SignInForm} />
          <Route path="/signup" component={SignUpForm} />
          <Route path="/igre" component = {GamePage} />
          <Route path="/dodaj" component={AddGameForm} />
          <Route path="/korisnici" component={UsersListPage} />
          
          <Route path="/novi-dogadjaj" component={StvoriDogadajForm} />
          <Route path="/profile" component={UserProfile} />
          <Route path='/dogadjaji/:eventId' component={EventVotingForm} />
          <Route path="/potvrde/:eventId" component={AttendantsForm} />
          <Route component={NotFoundPage} />
        </Switch>
      </div>
    </BrowserRouter>
  );
  
  export default AppRouter;
