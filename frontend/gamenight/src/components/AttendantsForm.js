import React from 'react';
import HeaderAdmin from './HeaderAdmin';
import AuthService from './auth/AuthService';
import DidntAttend from './DidntAttend';

class AttendantsForm extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            users: [],
            eventId: undefined
            
        }

        this.Auth = new AuthService();
    }

    componentDidMount(){
        let {eventId} = this.props.match.params;
        this.setState({eventId: eventId});
        
        fetch(`http://localhost:5000/dolazak/${eventId}`) 
        .then(res => {
            //console.log(res.body)
            return res.json()
            
        })
        .then(
          (result) => {
              console.log(result)
            this.setState({
              users: result
            });
            console.log(this.state.users);
          }
        ).catch(error => {
            //console.log(error.message)
        })
    }

    render(){

        

        const dataMapped = this.state.users.map((e => {
            return (
                
                    <div key={e.idKor} style={{color:"white", padding:"10px",  width: "500px", margin:"20px", borderStyle:"solid"}}>
                      <div style={{fontSize: "40pt"}}>{e.korisnickoIme}</div>
                      <hr style={{color:"white", borderStyle:"solid", borderWidth:"1px"}}></hr>
                      <div style={{fontSize: "25pt"}}>
                          Ime: {e.ime}<br />
                          Prezime: {e.prezime}<br />
                          Email: {e.email}
                      </div>
                    </div>
                
            );
          }));
        return(
            <div>
                <HeaderAdmin history={this.props.history} user={this.props.user} />
                <h1 style={{color:"white", margin:"20px"}}>Svi korisnici koji su potvrdili dolazak:</h1>
                <div><DidntAttend users={this.state.users} eventId={this.state.eventId} /></div>
                <div>{dataMapped}</div>
                
            </div>
            
        );
    }
}

export default AttendantsForm;