import React, { Component } from 'react';


class Footer extends Component {
  render() {
    return (
        <div id="footerDiv">
            <nav className="navbar fixed-bottom navbar-dark bg-dark">
            <div className="container-fluid">
             <label className="navbar-brand">Copyright: @Direwolves2018</label>
          </div>
            </nav>
        </div>
        );
    }
  }
  
  export default Footer;