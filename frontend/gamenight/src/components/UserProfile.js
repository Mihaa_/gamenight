import React from 'react';
import HeaderLoggedIn from './HeaderLoggedIn';
import {Progress} from 'reactstrap';

import withAuth from './auth/withAuth';
import HeaderAdmin from './HeaderAdmin';



class UserProfile extends React.PureComponent {

    render(){

        let color="danger";
        if(this.props.user.karma >= 0){
            color = "info"
        }

        let admin = this.props.user.username;
        let renderAdmin = false;
        if(admin==="admin"){
            renderAdmin=true
        }

        return(
            <div>

                {renderAdmin?<HeaderAdmin  history={this.props.history} user={this.props.user} /> : <HeaderLoggedIn history={this.props.history} user={this.props.user} />}
                
                <div className="profile">
                    <div className="profile username">{this.props.user.username}<hr /></div>
                    <div className="profile label">Ime: {this.props.user.name}<br />
                        Prezime: {this.props.user.lastName} <br />
                        E-mail: {this.props.user.email} <br />
                        Mobitel: {this.props.user.phone} <br />
                        {renderAdmin?'':"Karma:"}
                        {renderAdmin?'':<Progress animated color={color} value={this.props.user.karma>=0?this.props.user.karma:-this.props.user.karma} min={0} max={100}>{this.props.user.karma} bod</Progress>}
                    </div>
                </div>
            </div>
        );
    }

}

export default withAuth(UserProfile);