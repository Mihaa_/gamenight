import React from "react";
import Dogadaj from "./Dogadaj";
import { Link } from "react-router-dom";
import HeaderLoggedIn from "./HeaderLoggedIn";
import withAuth from './auth/withAuth';
import AuthService from './auth/AuthService';
import HeaderAdmin from './HeaderAdmin';

class DogadajList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          error: null,
          isLoaded: false,
          items: []
        };

        this.Auth = new AuthService();
        this.getInvitations = this.getInvitations.bind(this);
        this.getAllEvents = this.getAllEvents.bind(this);
    }

    getAllEvents(){

      console.log("admin")
      //POPRAVLJENO
      fetch(`http://localhost:5000/dogadjaj/svi`)
        .then(res => {
          if(res.status!==200){
            throw new Error("Nema ničega")
          }else{
            return res.json()
          }
          
        })
        .then(
          (result) => {
            console.log(result)
            let temp = []
            result.map(r=>{
              console.log(r)
              if(r.zavrsen){
                temp.push(r)
              }
            })

            this.setState({
              isLoaded: true,
              items: temp
            });
            //console.log(this.state.items);
          },
          (error => {
            console.log('nee');

            this.setState({
              isLoaded: true,
              error
            });
          })
        )

        console.log(this.state.items)

    }

    getInvitations(profile){

      console.log("ne admin")

      fetch(`http://localhost:5000/dogadjaj?id=${profile.sub}`) 
        .then(res => {

          if(res.status!==200){
            throw new Error("Nema ničega")
          }
          return res.json()
          
        })
        .then(
          (result) => {
            
            this.setState({
              isLoaded: true,
              items: result
            });
            console.log(this.state.items);
          },
          (error => {
            console.log('nee');

            this.setState({
              isLoaded: true,
              error
            });
          })
        ).catch(err=>{
          console.log(err.message)
        })

    }

    componentDidMount() {

      let profile = this.Auth.getProfile();
      
      profile.aud==="admin"?this.getAllEvents():this.getInvitations(profile);
    }

    render() {
      const { error, isLoaded, items } = this.state;

      let admin = this.props.user.username;
        let renderAdmin = false;
        if(admin==="admin"){
            renderAdmin=true
        }
        
        
      const dataMapped = this.state.items.map((e => {
        return (
          <div>
            <Dogadaj name={e.naziv} description={e.opis} status={e.privatni ? 'Privatni' : 'Javni'}/>
                {renderAdmin?<Link to={`/potvrde/${e.idDog}`} className="nav-link">
                <button type="button" className="btn btn-dark" >Pregledaj dolaske</button></Link>:<Link to={`/dogadjaji/${e.idDog}`} className="nav-link"> 
                      <button type="button" className="btn btn-dark" >Pregledaj događaj</button>
                  </Link>
                }
        
        </div>
        );
      }));


  
            return (
                <div>                
                    {renderAdmin?<HeaderAdmin  history={this.props.history} user={this.props.user} /> : <HeaderLoggedIn history={this.props.history} user={this.props.user} />}
                
                    
                    <h1 style={{color: "white"}}>{renderAdmin?'Svi završeni događaji':'Pozivnice'}</h1>
                    <div className="my-list">{dataMapped}</div>


                </div>
          );
          }
}

export default withAuth(DogadajList);