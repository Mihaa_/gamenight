import React from "react";
import { Link, NavLink } from "react-router-dom";
import {Alert} from 'react-bootstrap';
import AuthService from './auth/AuthService';

const emailRegex = RegExp(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);
const phoneRegex = RegExp(/^((?:\+|00)[17](?: |\\-)?|(?:\+|00)[1-9]\d{0,2}(?: |\\-)?|(?:\+|00)1\\-\d{3}(?: |\\-)?)?(0\d|\([0-9]{3}\)|[1-9]{0,3})(?:((?: |\\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\\-)[0-9]{3}(?: |\\-)[0-9]{4})|([0-9]{7}))/);

const formValid = ({formErrors, ...rest}) => {
    let valid = true;

    Object.values(formErrors).forEach(val => val.length > 0 && (valid = false));

    const entries = Object.entries(rest);

    for (const [key, value] of entries){

        if(key !== 'phone' && key !== 'alertType' && key !== 'alertMessage' && key !== 'terms'){
            if(value ==='' && (valid = false));
        }

        if(key === 'terms'){
            if(!value){
                valid = false;
            }
        }
    }
    
    return valid;
}

class SignUpForm extends React.Component {

    constructor() {
        super();

        this.state = {

            firstName: '',
            lastName: '',
            email: '',
            userName: '',
            password: '',
            phone: '',
            alertType: '',
            alertMessage: '',
            terms: false,
            formErrors: {

                email: '',
                password: '',
                firstName: '',
                lastName: '',
                userName: '',
                phone: '',
                terms: ''
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.Auth = new AuthService();
    }

    componentWillMount(){
        if(this.Auth.loggedIn())
            this.props.history.replace('/welcome');
    }

    handleChange(e) {

        //e.preventDefault();
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        //console.log(value);
        let name = target.name;

        let formErrors = this.state.formErrors;

        switch(name){

            case 'firstName':
                formErrors.firstName = value.length < 3 ? 'Potrebno najmanje 3 znaka' : '';
                break;

            case 'lastName':
                formErrors.lastName = value.length < 3 ? 'Potrebno najmanje 3 znaka' : '';
                break;

            case 'email':
                formErrors.email = emailRegex.test(value) ? '' : 'Nevaljana e-mail adresa';
                break;
            
            case 'password':
                formErrors.password = value.length < 6 ? 'Potrebno najmanje 6 znakova' : '';
                break;

            case 'phone':
                formErrors.phone = value.length === 0 || phoneRegex.test(value) ? '' : 'Nevaljan broj mobitela';
                break;

            case 'userName':
                formErrors.userName = value.length < 5 ? 'Potrebno najmanje 5 znakova' : '';
                break;

            case 'terms':
                formErrors.terms = !value ? 'Potrebno je prihvatiti uvjete' : '';
            break;

            default: break;


        }

        //console.log(formErrors.terms);

        this.setState({formErrors, [name]: value}, ()=> console.log(this.state));
    }

    handleSubmit(e) {
        e.preventDefault();

        if(formValid(this.state)){

            console.log(`The form was submitted with the following data->
            First Name: ${this.state.firstName}, 
            Last Name: ${this.state.lastName}, 
            Email: ${this.state.email},
            Username: ${this.state.userName},
            Password: ${this.state.password},
            Phone Number: ${this.state.phone},
            Terms: ${this.state.terms}
            `);

            fetch('http://localhost:5000/api/auth/signup', {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    ime: this.state.firstName,
                    prezime: this.state.lastName,
                    email: this.state.email,
                    korisnickoIme: this.state.userName,
                    lozinka: this.state.password,
                    mobitel: this.state.phone
                })
            })
            .then(response => {
                if(!response.ok && response.status!==400){
                    throw new Error(response.status);
                }
                else{
                    //console.log(response.json());
                    return response.json();
                }
            })
            .then(response => {

                console.log(response.success);
                if(response.success){
                    this.setState(
                        {alertType: "success",
                         alertMessage: "Registracija uspješna!"
                    });

                    //this.props.history.replace('/signin'); //tuuuuuuu
                }
                else {
                    this.setState(
                        {alertType: "warning",
                         alertMessage: "Korisnik već postoji!"
                    });
                }

            })
            .catch((e) => {
                console.log(e.message);
                this.setState(
                    {alertType: "warning",
                     alertMessage: "Nešto nije u redu!"
                    });
            });

        }else{

            this.setState(
                {alertType: "info",
                 alertMessage: "Obrazac nije pravilno popunjen!"
                });
        }
        
    }

    render() {

        const {formErrors} = this.state;

        const alertType = this.state.alertType;
        const alertMessage = this.state.alertMessage;
        let message;

        if(alertType==="success" || alertType==="warning" || alertType==="info"){
            message = <Alert bsStyle={alertType}>{alertMessage}</Alert>
        }
      
        return (
            <div className="sform">
                <div className="PageSwitcher">
                    <NavLink to="/signin" activeClassName="PageSwitcher__Item--Active" className="PageSwitcher__Item">Prijava</NavLink>
                    <NavLink exact to="/signup" activeClassName="PageSwitcher__Item--Active" className="PageSwitcher__Item">Registracija</NavLink>
                </div>

                <div className="FormCenter">
                    <form onSubmit={this.handleSubmit} className="FormFields" noValidate>
                        <div className="FormField">

                            <label className="FormField__Label" htmlFor="firstName">Ime</label>
                            <input type="text" id="firstName" className="FormField__Input" noValidate placeholder="Upišite ime" name="firstName" value={this.state.firstName} onChange={this.handleChange} />
                            <br />
                            {formErrors.firstName.length > 0 && (
                                <span className="errorMessage">{formErrors.firstName}</span>
                            )}
                        </div>

                        <div className="FormField">
                            <label className="FormField__Label" htmlFor="lastName">Prezime</label>
                            <input type="text" id="lastName" className="FormField__Input" noValidate placeholder="Upišite prezime" name="lastName" value={this.state.lastName} onChange={this.handleChange} />
                            <br />
                            {formErrors.lastName.length > 0 && (
                                <span className="errorMessage">{formErrors.lastName}</span>
                            )}
                        </div>

                        <div className="FormField">
                          <label className="FormField__Label" htmlFor="email">E-mail adresa</label>
                          <input type="email" id="email" className="FormField__Input" noValidate placeholder="Upišite e-mail adresu" name="email" value={this.state.email} onChange={this.handleChange} />
                          <br />
                          {formErrors.email.length > 0 && (
                                <span className="errorMessage">{formErrors.email}</span>
                        )}
                        </div>

                        <div className="FormField">
                            <label className="FormField__Label" htmlFor="userName">Korisničko ime</label>
                            <input type="text" id="userName" className="FormField__Input" noValidate placeholder="Upišite korisničko ime" name="userName" value={this.state.userName} onChange={this.handleChange} />
                            <br />
                            {formErrors.userName.length > 0 && (
                                <span className="errorMessage">{formErrors.userName}</span>
                            )}
                        </div>

                        <div className="FormField">
                            <label className="FormField__Label" htmlFor="phone">Broj mobitela (opcionalno)</label>
                            <input id="phone" className="FormField__Input" noValidate placeholder="Upišite broj mobitela" name="phone" value={this.state.phone} onChange={this.handleChange} />
                            <br />
                            {formErrors.phone.length > 0 && (
                                <span className="errorMessage">{formErrors.phone}</span>
                            )}
                        </div>
            
                        <div className="FormField">
                            <label className="FormField__Label" htmlFor="password">Lozinka</label>
                            <input type="password" id="password" className="FormField__Input" noValidate placeholder="Upišite lozinku" name="password" value={this.state.password} onChange={this.handleChange} />
                            <br />
                            {formErrors.password.length > 0 && (
                                <span className="errorMessage">{formErrors.password}</span>
                            )}
                        </div>

                        <div className="FormField">
                            <label className="FormField__Label" htmlFor="password">Prihvaćam uvjete korištenja</label>
                            <input type="checkbox" id="checkbox" name="terms" checked={this.state.terms} onChange={this.handleChange} />
                            <br />
                            {formErrors.terms.length > 0 && (
                                <span className="errorMessage">{formErrors.terms}</span>
                            )}
                        </div>
                        
                        <div className="FormField">
                            <button type="submit" className="FormField__Button mr-20">Registracija</button> <br />
                            <Link to="/signin" className="FormField__Link">Imam račun!</Link> <br />
                            <Link to="/" className="FormField__Link">Povratak na početnu stranicu</Link>
                        </div>
                    </form>
                </div>
                {message}
                                 
            </div>
        );
    }
  }
  
  export default SignUpForm;