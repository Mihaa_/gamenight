import React from "react";

const Loader = () => {
  return (
    <h1 className="loader">
      <div className="lds-dual-ring" />
      <span>Loading</span>
    </h1>
  );
};

export default Loader;