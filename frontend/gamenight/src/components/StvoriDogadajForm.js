import React from 'react';
import {DateRangePicker} from 'react-dates';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Form, FormGroup, Container} from 'reactstrap';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import  MultiSelectReact  from 'multi-select-react';
import HeaderLoggedIn from './HeaderLoggedIn';
import {Alert} from 'react-bootstrap';
import moment from 'moment';
import AuthService from './auth/AuthService';

import withAuth from './auth/withAuth';
import HeaderAdmin from './HeaderAdmin';

class StvoriDogadajForm extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state ={
          date: null,
          startDate: null,
          endDate: null,
          focusedInput: null,
          focused: null,
          korisnici: [],
          multiSelect: [],
          imeDog: '',
          opisDog: '',
          response: null,
          event: null,
          korisnici: null,
          adminChoice: false
        }

        this.Auth = new AuthService();
        this.handleChange = this.handleChange.bind(this);

    }

    handleChange(e) {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
          [name]: value
        });

    }


    componentWillMount() {

        fetch('http://localhost:5000/api/korisnici')
        .then(results => {
            return results.json();
        })
        .then(data => {

            
            let kor = []

            this.setState({korisnici: data});

            for (var i = 0; i < data.length; i++) {
                if(data[i].korisnickoIme!=="admin"){
                    kor.push({label: data[i].korisnickoIme, id: data[i].idKor});
                }
               
            }
            
            this.setState({multiSelect: kor, korisnici: kor});
            
            
            console.log("state: ", this.state.multiSelect);
            

        })
        .catch(error => {
            console.log("Error");   
            this.setState({error: true});
        });
    }

    addEvent() {


        let isAdmin = this.props.user.username==="admin"?true:false;
        let privacy = '';
        if(isAdmin && this.state.adminChoice){
            privacy = "javni"
        }else{
            privacy = "privatni"
        }

        return fetch(`http://localhost:5000/dogadjaj/${privacy}`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    idDog: 100000,
                    datumIsteka: this.state.startDate,
                    naziv: this.state.imeDog,
                    opis: this.state.opisDog,
                    privatni: !this.state.adminChoice
                })
            })
            .then(response => {
                this.setState({
                    response: response
                })
                console.log(response);
                if (!response.ok) {
                    throw new Error(response.status);
                } else return response.json();
            })
            .then(() => {
                this.setState({
                    alertType: "success",
                    alertMessage: "Uspješno napravljen događaj!"
                });
                console.log(this.state.alertMessage);

            })
            .catch((error) => {
                let e = String(error);
                this.setState({
                    alertType: e.includes('JSON') ? "success" : "warning",
                    alertMessage: !e.includes('JSON') ? "Niste uspješno napravili događaj!" : "Uspješno napravljen događaj!"
                });
                console.log(this.state.alertMessage);
            });
    }

    findEventByName() {
        if (this.state.alertType === 'warning') {
            // Promise.reject();
            return;
        }
        console.log(this.state.imeDog);
        
        return fetch(`http://localhost:5000/dogadjaj/naziv/` + this.state.imeDog)
        .then(results => {
            return results.json();
        })
        .then(data => {
    
            this.setState({event: data});

            console.log(this.state.event);

        })
        .catch(error => {
            let e = String(error);
            console.log(e);
            
            this.setState({
                alertType: e.includes('JSON') ? "success" : "warning",
                alertMessage: !e.includes('JSON') ? "Niste uspješno napravili događaj!" : "Uspješno napravljen događaj!"
            });        
            console.log(this.state.alertMessage);   
        });
    }

    sendInvitations() {
        if (this.state.alertType === 'warning') {
            // Promise.reject()
            return;
        }        
        let end;
        for (var i = 0; i < this.state.multiSelect.length; i++) {
            if (this.state.multiSelect[i].value === true) {
                console.log(JSON.stringify({
                    idDog: this.state.event.idDog,
                    idKor: this.state.multiSelect[i].id,
                }));
                end = fetch('http://localhost:5000/dogadjaj/privatni/pozivnice', {
                    method: 'POST',
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        idDog: this.state.event.idDog,
                        idKor: this.state.multiSelect[i].id,
                    })
            })
            .then(response => {
                if(!response.ok){
                    throw new Error(response.status);
                }
                else return response.json();
            })
            .then(() => {
                this.setState(
                    {alertTypeDays: "success",
                     alertMessageDays: "Pozivnica uspješno poslana!"
                    });
                    console.log(this.state.alertMessageDays);
                    if (this.state.showError !== "warning") {
                        this.setState({showError: "success"})
                    }
            })
            .catch((error) => {
                let e = String(error);
                this.setState(
                    {alertTypeGames: "warning",
                     alertMessageGames: e.includes('500') ? "Pozivnica nije uspješno pozvana!" : "Pozivnica uspješno poslana!"
                    });
                    if (this.state.showError !== "warning") {
                        this.setState({showError: e.includes('500') ? "warning" : "success"})
                    }
                    console.log(this.state.alertMessageGames);
            });
    
            }

            if (this.state.alertType === 'warning') {
                return;
            }     
        }
        return end;
    }

    napraviDogadaj(e) {
        
        console.log(this.state.adminChoice)
        var start = moment(this.state.startDate);
        var end = moment(this.state.endDate);
        
        var diff = end.diff(start, 'days') // 6
        
        if (this.state.opisDog === '' || this.state.opisDog === null || this.state.imeDog === '' || this.state.imeDog === null || diff !== 6) {
            this.setState({alertType: "warning"});
            return;
        }

        e.preventDefault();

        
        this.addEvent(/*...*/).then(result => this.findEventByName(/*...*/)).then(result => this.sendInvitations()).catch(/*...error handler...*/);
  

    }

    render() {


        const selectedOptionsStyles = {
            color: "#3c763d",
            backgroundColor: "#dff0d8"
        };
        const optionsListStyles = {
            backgroundColor: "#dff0d8",
            color: "#3c763d"
        };

        let isAdmin = this.props.user.username==="admin"?true:false;

        
        let message;

        if(this.state.alertType==="warning"){
            message = <Alert id="event-alert" bsStyle="warning">Događaj nije uspješno napravljen.</Alert>
        }
        else if (this.state.alertType==="success"){
            // this.setState({imeDog: '', startDate: null, endDate: null, opisDog: '', multiSelect: this.state.korisnici})

            message = <Alert id="event-alert" bsStyle="success">Događaj je uspješno napravljen.</Alert>
        }

     return (
         <div>{isAdmin?<HeaderAdmin history={this.props.history} user={this.props.user} />:<HeaderLoggedIn history={this.props.history} user={this.props.user}/>}
              
      <div className="container-fluid">

       <form>
        <fieldset>
         <h1 id="kreirDogH1">Kreiranje događaja</h1>
          <div className="form-group">
           <div className="col-md-4" >
           <label htmlFor="labelDogadaj">Ime događaja</label>
           <input className="form-control" id="inputDogadaj" name="imeDog" aria-describedby="nameHelp" 
           placeholder="Unesi ime događaja" value={this.state.imeDog} onChange={this.handleChange}></input>
           </div>
          </div>
          
          <div className="form-group">
           <div className="col-md-4">
            <label htmlFor="exampleTextarea">Opis događaja</label>
            <textarea className="form-control" id="opisDogadaja" rows="3"
            placeholder="Ovdje opiši svoj događaj i privuci suigrače!" name="opisDog" value={this.state.opisDog} onChange={this.handleChange}></textarea>
           </div>
          </div>
          
          <div className="form-group">
           <div className="col-md-4">
            <label htmlFor="exampleTextarea">Odaberi tjedan održavanja<p id="opisTjedanOdrzavanja"> (točno 7 dana, npr. od ponedjeljka do nedjelje)</p></label>

            <div>
        <Container>
          <Form>
            <FormGroup>
            <DateRangePicker
  startDate={this.state.startDate} // momentPropTypes.momentObj or null,
  startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
  endDate={this.state.endDate} // momentPropTypes.momentObj or null,
  endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
  onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })} // PropTypes.func.isRequired,
  focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
  onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
  minDate={Date.now()}
/>
  </FormGroup>
          </Form>
        </Container>
      </div>
           </div>
          </div>


          <div className="form-group">
           <div className="col-md-4">
           {this.state.adminChoice?null:<div>
            <label htmlFor="exampleTextarea">Pozovi korisnika</label>
            <MultiSelectReact 

      options={this.state.multiSelect}
      optionClicked={this.optionClicked.bind(this)}
      selectedBadgeClicked={this.selectedBadgeClicked.bind(this)}
      selectedOptionsStyles={selectedOptionsStyles}
      optionsListStyles={optionsListStyles} />

           </div>}
           </div>
          </div>

          <div className="form-group">
           <div className="col-md-4">
            <button type="button" className="btn btn-secondary"
            onClick={this.napraviDogadaj.bind(this)} >Napravi događaj</button>

            
           </div>
          </div>

            {
                isAdmin?<div className="form-group">
                <div className="col-md-4" style={{color:"white", fontSize:"18pt"}}>
                 <input type="checkbox" 
                 onChange={this.handleChange} name="adminChoice"></input> Želim javni događaj
                </div>
               </div>:''
            }
          <div className="glasaj">          {message}

          </div>
        </fieldset>
       </form>
      </div>
      </div>
        );
    }
    

    optionClicked(optionsList) {
        this.setState({ multiSelect: optionsList });
    }
    
    selectedBadgeClicked(optionsList) {
        this.setState({ multiSelect: optionsList });
    }
    


}
export default withAuth(StvoriDogadajForm);
