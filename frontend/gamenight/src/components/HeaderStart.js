import React, { Component } from 'react';
import { Link } from "react-router-dom";



class HeaderStart extends Component {
  render() {
    return (
      <div className="HeaderStart">
        
          <header id="mainHeader" className="container-fluid">  
            <nav id="headerBar" className="navbar fixed-top navbar-dark bg-dark">
              
                <Link to="/" className="navbar-brand">
                <img src="..\icons8-nintendo-gamecube-controller-96.png" width="50" height="50" alt="" />
                GameNight
                </Link>
                <ul className="nav navbar-right">
                  <li className="nav-item">
                  <Link to="/signup" className="nav-link"> 
                    <button type="button" className="btn btn-secondary btn-lg"> Registracija</button>
                  </Link></li>
                  <li className="nav-item">
                    <Link to="/signin" className="nav-link"> 
                      <button type="button" className="btn btn-secondary btn-lg"> Prijava</button>
                    </Link>
                  </li>
                </ul>
              
            </nav>
        </header>
    </div>
        );
  }
}

export default HeaderStart;
/*
<ul class="nav justify-content-center">
                  <li class="nav-item">
                  <Link class="nav-link" to="/">
                    <button type="button" class="btn btn-secondary">Pocetna stranica</button>
                  </Link>
                  </li>
                  <li class="nav-item">
                  <Link class="nav-link" to="#">
                    <button type="button" class="btn btn-secondary">Događaji</button>
                  </Link>
                  </li>
                  <li class="nav-item">
                  <Link class="nav-link" to="#">
                    <button type="button" class="btn btn-secondary">Igre</button>
                  </Link>
                  </li>
                </ul>
                */