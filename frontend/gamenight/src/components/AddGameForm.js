import React from "react";
import {Link} from 'react-router-dom';
import HeaderLoggedIn from './HeaderLoggedIn';
import {Alert} from 'react-bootstrap';
import withAuth from './auth/withAuth';

import AuthService from './auth/AuthService';
import HeaderAdmin from "./HeaderAdmin";
class AddGameForm extends React.Component{

    constructor(props){
        super(props);

        this.state = {

            naziv: '',
            opis: '',
            link: '',
            alertType: '',
            alertMessage: '',
            isAdmin: undefined
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.Auth = new AuthService();
    }

    componentDidMount(){
        let profile = this.Auth.getProfile();

        if(profile.aud==="admin"){
            this.setState({isAdmin: true})
        }
        else{
            this.setState({isAdmin: false})
        }
    }

    handleChange(e) {
        let target = e.target;
        let value = target.value;
        let name = target.name;

        this.setState({
          [name]: value
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        if (this.state.naziv === '' || this.state.opis === '' || this.state.link === '') {
            this.setState({alertType: 'warning', alertMessage: 'Nisu ispunjena sva polja. '});
            return;
        }

        fetch('http://localhost:5000/api/igre', {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    naziv: this.state.naziv,
                    opis: this.state.opis,
                    link: this.state.link,
                })
        })
        .then(response => {
            if(!response.ok){
                throw new Error(response.status);
            }
            else return response.json();
        })
        .then(() => {
            this.setState(
                {alertType: "success",
                 alertMessage: "Igra uspješno dodana"
                });
        })
        .catch(() => {
            this.setState(
                {alertType: "warning",
                 alertMessage: "Igra s tim imenom već postoji!"
                });
        });
        
    }

    render(){

        const alertType = this.state.alertType;
        const alertMessage = this.state.alertMessage;
        let message;

        if(alertType==="success" || alertType==="warning"){
            message = <Alert bsStyle={alertType}>{alertMessage}</Alert>
        }
        
        let admin;
        if(this.state.isAdmin){
            admin=true;
        }else{
            admin=false;
        }

        return(
            <div>
                {admin?<HeaderAdmin history={this.props.history} user={this.props.user} />:<HeaderLoggedIn history={this.props.history} user={this.props.user} />}
                
                <div className="sform">
                    <h2>Dodavanje igre</h2><br />
                    <form onSubmit={this.handleSubmit} noValidate>

                        <div>
                            <label className="FormField__Label">Naziv: </label>
                            <input className="FormField__Input" type="text" id="naziv" noValidate placeholder="Upišite naziv igre" name="naziv" value={this.state.naziv} onChange={this.handleChange} /><br />
                        </div>

                        <div>
                            <label className="FormField__Label">Opis</label>
                            <input className="FormField__Input" type="text" id="opis" noValidate placeholder="Upišite opis igre" name="opis" value={this.state.opis} onChange={this.handleChange} /><br />
                        </div>
                        <br />
                        <div>
                            <label className="FormField__Label">Link</label>
                            <input className="FormField__Input" type="text" id="link" noValidate placeholder="Upišite link igre" name="link" value={this.state.link} onChange={this.handleChange} /><br />
                        </div>

                        <br />

                        <button type="submit" className="FormField__Button mr-20">Dodaj</button><br /><br />
                        <Link to="/igre"><button className="FormField__Button mr-20">Povratak na igre</button></Link>
                        <br />
                        <br />
                        {message}
                    </form>
                </div>
            </div>
        );


    }

}

export default withAuth(AddGameForm);