import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import  MultiSelectReact  from 'multi-select-react';
import HeaderLoggedIn from './HeaderLoggedIn';
import moment from 'moment';
import {Alert} from 'react-bootstrap';
import AuthService from './auth/AuthService';

class EventVotingForm extends Component {

    

    constructor(props) {
        
        super(props);
        this.state ={
          isLoaded: null,
          event: null,
          error: null,
          multiSelectDays: [],
          multiSelectGames: [],
          games: null,
          results: [],
          array: [],
          alertTypeGames: null,
          alertMessageGames: null,
          alertTypeDays: null,
          alertMessageDays: null,
          showError: null,
          status: null,
          idUser: null
        }

        this.Auth = new AuthService();
        
    }
    componentDidMount() {
       
        let profile = this.Auth.getProfile();
        
        this.setState({idUser: profile.sub});

        let {eventId} = this.props.match.params;

        fetch(`http://localhost:5000/dogadjaj/${eventId}`) 
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                isLoaded: true,
                event: result
              });

              this.setState({status: this.state.event.privatni ? 'Privatni' : 'Javni'})
              let date =  moment(this.state.event.datumIsteka).format('YYYY-MM-DD');
            //   let date2 =  moment(date, 'DD.MM.YYYY').format('YYYY-MM-dd');
              //console.log(this.state.event);
              
              let datumi = [];
              for (var i = 0; i < 7; i++) {
                  datumi.push({label: date});
                  let newDate = moment(date, 'YYYY-MM-DD').add(1, 'days')._d;
                  date = moment(newDate).format('YYYY-MM-DD');
              }
              this.setState({multiSelectDays: datumi});
            },
            (error) => {
              console.log('nee');
              this.setState({
                isLoaded: true,
                error
              });
            }
          )

          fetch('http://localhost:5000/api/igre')
          .then(results => {
              return results.json();
          })
          .then(data => {
              let games = data.map(g => {
                  return g.naziv;
              })
              let array = [];
              this.setState({results: games});
              for (var i = 0; i < data.length; i++) {
                  array.push({label: data[i].naziv, id: data[i].idIgre});
              }
              this.setState({multiSelectGames: array});
  
          })
          .catch(error => {
              console.log("Error");   
              this.setState({error: true});
          });
        
    
      }


      
      render() {
        const isLoaded = this.state.isLoaded;

        

        if (!isLoaded) {
            return (
                <div>Loading...</div>
            )
        }
        if (this.state.event.zavrsen) {
            return (
                <div>
         <HeaderLoggedIn history={this.props.history} user={this.props.user} />
                

      <div className="container-fluid">
         <h1 id="kreirDogH1">{isLoaded ? this.state.event.naziv : ""}</h1>
         <p id="opis">{isLoaded ? this.state.event.opis : ""}</p>
          <div className="form-group">
           <div className="col-md-4" >
           <p id="crveni">Događaj je završen. </p>
        </div>
          </div>

          </div>
          </div>
            );
        }

        else if (this.state.event.izglasan) {
            let message;
            if (this.state.showError==="warning"){
                message = <Alert id="event-alert" bsStyle="warning">Niste uspješno potvrdili dolazak.</Alert>
            }
            else if (this.state.showError==="success"){
                message = <Alert id="event-alert" bsStyle="success">Uspješno potvrđen dolazak.</Alert>
            }
            return (
                <div>
        <HeaderLoggedIn history={this.props.history} user={this.props.user} />
                

      <div className="container-fluid">
         <h1 id="kreirDogH1">{isLoaded ? this.state.event.naziv : ""}</h1>
         <p id="opis">{isLoaded ? this.state.event.opis : ""}</p>
         <div className="form-group">
           <div className="col-md-4" >
           <label htmlFor="labelDogadaj">Igra koja se igra na događaju: </label>
           <p id="opis">{isLoaded ? this.state.event.igra.naziv : ""}</p>
        </div>
          </div>
          <div className="form-group">
           <div className="col-md-4" >
           <label htmlFor="labelDogadaj">Izabrani dan događaja: </label>
           <p id="opis">{isLoaded ? moment(this.state.event.datumIsteka).format('DD.MM.YYYY') : ""}</p>
        </div>
          </div>
          <div className="form-group">
           <div className="col-md-4" >
           <label htmlFor="labelDogadaj">Odluči želiš li sudjelovati: </label>
           <button type="button" className="btn btn-dark" onClick={this.onZelimSudjelovati.bind(this)}>Da, želim sudjelovati</button>
        </div>
        <div className="glasaj">{message}

        </div></div>
          </div>
          </div>
            );
        }
        else {
        const selectedOptionsStyles = {
            color: "#3c763d",
            backgroundColor: "#dff0d8"
        };
        const optionsListStyles = {
            backgroundColor: "#dff0d8",
            color: "#3c763d"
        };
        const alertType = this.state.alertType;
        const alertMessage = this.state.alertMessage;
        let message;

        if(this.state.showError==="warning"){
            message = <Alert id="event-alert" bsStyle="warning">Već ste glasali za neku od igri ili dana. Igre ili dani za koje niste glasali uspješno su unešene.</Alert>
        }
        else if (this.state.showError==="success"){
            message = <Alert id="event-alert" bsStyle="success">Uspješno glasanje.</Alert>
        }
        else if (this.state.showError==="error"){
            message = <Alert id="event-alert" bsStyle="warning">Morate glasati za barem jednu igru i barem jedan dan.</Alert>
        }
        
     return (
         <div>
        <HeaderLoggedIn history={this.props.history} user={this.props.user} />
                
      <div className="container-fluid">

       <form>
        <fieldset>
         <h1 id="kreirDogH1">{isLoaded ? this.state.event.naziv : ""}</h1>
         <p id="opis">{isLoaded ? this.state.event.opis : ""}</p>
          <div className="form-group">
           <div className="col-md-4" >
           <label htmlFor="labelDogadaj">Odaberi dane održavanja</label>
           <MultiSelectReact 
      options={this.state.multiSelectDays}
      optionClicked={this.optionClickedDays.bind(this)}
      selectedBadgeClicked={this.selectedBadgeClickedDays.bind(this)}
      selectedOptionsStyles={selectedOptionsStyles}
      optionsListStyles={optionsListStyles} />
        </div>
          </div>
          
          <div className="form-group">
           <div className="col-md-4">
            <label htmlFor="exampleTextarea">Odaberi igre</label>
            <MultiSelectReact 
      options={this.state.multiSelectGames}
      optionClicked={this.optionClickedGames.bind(this)}
      selectedBadgeClicked={this.selectedBadgeClickedGames.bind(this)}
      selectedOptionsStyles={selectedOptionsStyles}
      optionsListStyles={optionsListStyles} />
        </div>
          </div>

          <div className="glasaj"><button type="button" className="btn btn-dark" onClick={this.onGlasaj.bind(this)}>Glasaj</button>
          </div>
          <div className="glasaj">          {message}

          </div>
        </fieldset>
       </form>
      </div>
      </div>
        );
        }
    
    }

    onZelimSudjelovati() {

        let {eventId} = this.props.match.params;
        console.log(eventId)
        
        fetch('http://localhost:5000/dolazak', {
                    method: 'POST',
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        idDog: eventId, //fake podaci, ovo ionak ne radi
                        idKor: this.state.idUser
                    })
            })
            .then(response => {
                if(!response.ok){
                    throw new Error(response.status);
                }
                else return response.json();
            })
            .then(() => {
                this.setState(
                    {alertType: "success",
                     alertMessage: "Uspješno potvrđen dolazak!"
                    });
                   
            })
            .catch((error) => {
                let e = String(error);
                this.setState(
                    {alertType: !e.includes('JSON') ? "warning" : "success",
                     alertMessage: "Niste uspješno potvrdili dolazak!",
                     showError: !e.includes('JSON') ? "warning" : "success"
                    });

                    
            });
    }

    onGlasaj() {
        this.setState({showError: ""})
        let d = 0; 
        for (var i = 0; i < this.state.multiSelectDays.length; i++) {
            if (this.state.multiSelectDays[i].value === true) {
                d++;
            }
        }

        let g = 0; 
        for (var i = 0; i < this.state.multiSelectGames.length; i++) {
            if (this.state.multiSelectGames[i].value === true) {
                g++;
            }
        }

        if (d === 0 || g === 0) {
            this.setState({showError: "error"});
            return;
        }
        
        //ZA IGRE
        for (var i = 0; i < this.state.multiSelectGames.length; i++) {
            if (this.state.multiSelectGames[i].value === true) {
                
                fetch('http://localhost:5000/dogadjaj/glasanje/igra', {
                    method: 'POST',
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        idDog: this.state.event.idDog,
                        idKor: this.state.idUser,
                        idIgra: this.state.multiSelectGames[i].id,
                        tezinaGlasa: 0
                    })
            })
            .then(response => {
                if(!response.ok){
                    throw new Error(response.status);
                }
                else return response.json();
            })
            .then(() => {
                this.setState(
                    {alertType: "success",
                     alertMessage: "Glasanje uspješno!"
                    });
                    
                    if (this.state.showError !== "warning") {
                        this.setState({showError: "success"})
                    }
                    
            })
            .catch((error) => {
                let e = String(error);
                this.setState(
                    {alertType: "warning",
                     alertMessage: e.includes('500') ? "Niste uspješno glasali!" : "Glasanje uspješno"
                    });
                   
                    
                    if (this.state.showError !== "warning") {
                        
                        
                        this.setState({showError: e.includes('500') ? "warning" : "success"})
                    }

                    
            });
    
            }
        }

        // za DANE
        for (var i = 0; i < this.state.multiSelectDays.length; i++) {
            if (this.state.multiSelectDays[i].value === true) {
                
                fetch('http://localhost:5000/dogadjaj/glasanje/dan', {
                    method: 'POST',
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        idDog: this.state.event.idDog,
                        idKor: this.state.idUser,
                        tezinaGlasa: 0,
                        dan: moment(this.state.multiSelectDays[i].label),
                    })
            })
            .then(response => {
                if(!response.ok){
                    throw new Error(response.status);
                }
                else return response.json();
            })
            .then(() => {
                this.setState(
                    {alertTypeDays: "success",
                     alertMessageDays: "Glasanje uspješno!"
                    });
                    
                    if (this.state.showError !== "warning") {
                        this.setState({showError: "success"})
                    }
            })
            .catch((error) => {
                let e = String(error);
                this.setState(
                    {alertTypeGames: "warning",
                     alertMessageGames: e.includes('500') ? "Niste uspješno glasali!" : "Glasanje uspješno"
                    });
                    if (this.state.showError !== "warning") {
                        this.setState({showError: e.includes('500') ? "warning" : "success"})
                    }
                   
            });
    
            }
        }
        
    }

    optionClickedDays(optionsList) {
        this.setState({ multiSelectDays: optionsList });
    }
    
    selectedBadgeClickedDays(optionsList) {
        this.setState({ multiSelectDays: optionsList });
    }

    optionClickedGames(optionsList) {
        
        
        this.setState({ multiSelectGames: optionsList });
    }
    
    selectedBadgeClickedGames(optionsList) {
        

        this.setState({ multiSelectGames: optionsList });
    }

}

export default EventVotingForm;
