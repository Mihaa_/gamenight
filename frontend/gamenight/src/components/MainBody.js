import React, { Component } from 'react';
import HeaderStart from './HeaderStart';
import Footer from './Footer';
import Gallery from './Gallery';


class MainBody extends Component {
  render() {
    return (

        
            <div id="pocetna" className="container">
                <HeaderStart />
                <div className="titleStart">
                    <h1>GameNight</h1>
                    <hr />
                </div>
                <div className="gallery">
                    <Gallery />
                </div>

                <div className="row" style={{color: "white", fontSize: "18pt", marginTop: "20px", textAlign: "justify", marginBottom: "50px"}}>
                    <p>Postoje mnoga profesionalna natjecanja i turniri širom svijeta koji okupljaju milijune igrača kroz čitav spektar
                        računalnih igara. <br />Javlja se problem kod neprofesionalnih igrača koji bi isto tako htjeli prisustvovati
                        natjecanjima gdje zapravo ne postoji način kako bi se oni okupili, dogovorili i na posljetku odigrali
                        neku igru u opuštenom okruženju.<br /> Rješenje: aplikacija koja omogućuje
                        kreiranje događaja u obliku druženja uz igranje igara ili nekog oblika turnira
                        ili natjecanja, glasanje za datum održavanja i za igru koju bi hitjeli igrati
                        na tom događaju. 
                    </p>
                </div>
                <Footer />
            </div>
        
        );
  }
}

export default MainBody;