import React from 'react';
import AuthService from './AuthService';

export default function withAuth(AuthComponent) {
    
    const Auth = new AuthService('http://localhost:5000');

    return class AuthWrapped extends React.PureComponent {

        _isMounted = false;

        componentWillUnmount() {
          this._isMounted = false;
        }

              
        constructor() {
            super();
            this.state = {
                success: null,
                user: {
                    idUser: 0,
                    username: '',
                    email: '',
                    name: '',
                    lastName: '',
                    karma: 0,
                    phone: ''
                }
            }

            this.getUser = this.getUser.bind(this);
        }

        getUser(profile){

            //console.log(`id je ${profile.sub}`);
            
            fetch(`http://localhost:5000/api/korisnici/${profile.sub}`)
            .then(results =>{
                return results.json()
            })
            .then(data => {
                
                if (this._isMounted){
                    this.setState({
                        success: 'uspjeh',
                        user: {
                            idUser: data.idKor,
                            username: data.korisnickoIme,
                            email: data.email,
                            name: data.ime,
                            lastName: data.prezime,
                            karma: data.karma,
                            phone: data.mobitel
                        }
                    });
                }
                
            })
        }

        componentDidMount() {
            this._isMounted = true;

            if (!Auth.loggedIn()) {
                this.props.history.replace('/')
            }
            else {
                try {
                    const profile = Auth.getProfile()
                    
                    this.getUser(profile);
                }
                catch(err){
                    Auth.logout()
                    this.props.history.replace('/')
                }
            }
        }

        render() {
            
            if (this.state.user) {
                //console.log(this.state.success)
                return (
                    <AuthComponent history={this.props.history} user={this.state.user} />
                )
            }
            else {
                return null
            }
        }
    }
}