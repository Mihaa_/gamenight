import React from 'react';
import HeaderStart from './HeaderStart';

class Home extends React.Component{

    render(){

        return(

            <div>
                <HeaderStart />
                <div className="titleStart">
                    <h1>GameNight</h1>
                    <hr />
                </div>
            </div>
        );
    }


}
export default Home;