import React from 'react';
import UserList from './results/UserList';

import withAuth from './auth/withAuth';
import HeaderAdmin from './HeaderAdmin';

class UsersListPage extends React.Component {

    render(){

        return(

            <div>
               <HeaderAdmin history={this.props.history} user={this.props.user} />
               
                <UserList />

               
            </div>

        );
    }

}

export default withAuth(UsersListPage);