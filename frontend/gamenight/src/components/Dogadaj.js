import React, { Component } from 'react';


class Dogadaj extends Component {
    
    constructor(props) {
        super(props);
        
        this.state={
            i:0,
            slike:[
            "http://www.ubuntu-ast.org/largeimages/12/129320_gaming-live-wallpaper-for-pc.jpg",
            "https://k09.kn3.net/C7DFCA64B.jpg",
            "https://images4.alphacoders.com/243/243838.jpg",
            "https://www.pixelstalk.net/wp-content/uploads/2016/05/Desktop-gaming-backgrounds-1080p-dosh-tosh.jpg",    
            "https://data.1freewallpapers.com/download/bambi-jungle.jpg",
            "https://wallup.net/wp-content/uploads/2017/05/29/381445-landscape-nature-fantasy_art-artwork.jpg",
            "https://images.alphacoders.com/137/137093.jpg",
            "https://tubularinsights.com/wp-content/uploads/2015/11/gaming-on-youtube.jpg",
            "https://i.imgur.com/cGkdxIO.jpg",
            "https://compass-ssl.xbox.com/assets/dc/48/dc486960-701e-421b-b145-70d04f3b85be.jpg?n=Game-Hub_Content-Placement-0_New-Releases-No-Copy_740x417_02.jpg"
        ]};
        
        

    }

    componentDidMount() {

        var rand=Math.floor(Math.random() * (this.state.slike.length));
        this.setState({
            i:rand
        });

    }
    
    render() {
        const { name, description, status } = this.props;


     return (
      <div className="container-fluid">
        <h1>{name}</h1>
        <div className="row">
        <div className="col-md-6">
            <img src={ 
                this.state.slike[this.state.i]
                } height="250px" width="100%" alt=""></img>
        </div>
        <div className="col-md-6">
            <div className="card text-white bg-dark mb-3" >
                <div id="status" className="card-header">{status}</div>
                <div className="card-body">
                    <p className="card-text">{description}</p>
                </div>
            </div>
        </div>
        </div>

      </div>
        );
    }
}

export default Dogadaj;