import React from "react";
import withAuth from './auth/withAuth';
import Select from 'react-select';
import * as emailjs from 'emailjs-com';

import {Alert} from 'react-bootstrap';

class BanUser extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            results: [],
            sendEmailTo: '',
            emailId: undefined,
            alertMessage: '',
            alertType: ''
        }

        this.sendEmail = this.sendEmail.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.getOptions = this.getOptions.bind(this);
        this.udalji = this.udalji.bind(this);
    }

    sendEmail(){
        console.log("Šaljem mail-> "+this.state.sendEmailTo);

        var myDate = new Date();
        myDate.setDate(myDate.getDate() + 5);

        var day = myDate.getDate();
        var month = myDate.getMonth()
        var year = myDate.getFullYear()

        var templateParams = {
            from_name: "GameNight",
            to_name: this.state.sendEmailTo,
            subject: "[GameNight] Udaljavanje od aplikacije",
            message_html: day+"."+month+1+"."+year+"."
        }

        emailjs.send("gmail", "template_T9B8fNwK", templateParams, "user_dtVRyUq8OVlaQuUVV5mVk")
            .then(res =>{
                console.log("SUCCESS!", res.status, res.text)
            })
            .catch(err =>{
                console.log(err.message)
            })
    }

    udalji(){

        var myDate = new Date();
        myDate.setDate(myDate.getDate() + 5);
        
        fetch('http://localhost:5000/udaljavanje', {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    idKor: this.state.emailId,
                    vrijemeKazne: myDate
                })
            })
            .then(response => {
                if(!response.ok && response.status!==400){
                    
                    throw new Error(response.status);
                }
                else{
                    console.log(response)
                    return response.json();
                }
            })
            .then(response => {

                
                this.setState({alertMessage:"Uspjeh!", alertTytpe:"success"});
                
            })
            .catch(err=>{
                this.setState({alertMessage:"Uspjeh!", alertType:"success"});
            })
            



        this.sendEmail();
    }

    getOptions(){
        let options = []
        this.state.results.map(user => {
            if(user.korisnickoIme!=="admin"){
                options.push({value: user.email, label: user.korisnickoIme, key: user.idKor});
               
            }
        })
        return options;

    }

    handleChange(event){
        //console.log(event)
        this.setState({sendEmailTo: event.value, emailId: event.key})
    }

    componentWillMount(){


        fetch('http://localhost:5000/api/korisnici')
        .then(results => {
            return results.json();
        })
        .then(data => {
            this.setState({results: data});

        })
        .catch(error => {
            //console.log("Error");   
            this.setState({error: true});
        });
    }

    render(){

        let opt = this.getOptions();

        const alertType = this.state.alertType;
        const alertMessage = this.state.alertMessage;
        let message;

        if(alertType==="success" || alertType==="warning" || alertType==="info"){
            message = <Alert bsStyle={alertType}>{alertMessage}</Alert>
        }
        

        return(
            <div>
                <div style={{display: "inline", marginRight:"10px", width: "20px"}}>
                {message}
                    <Select 
                        className="select" 
                        options={opt} 
                        onChange={this.handleChange} 
                        placeholder="Odaberite korisnika"
                    />
                       
                </div>   
            <div style={{display: "inline"}}>
            
                <button onClick={this.udalji} type="button" className="btn btn-secondary btn-md">
                    <img width="30px" height="30px" alt=""
                        src="https://www.iconsdb.com/icons/preview/red/ban-xxl.png"></img>
                        Udalji korisnika
                </button>
                
            </div> 
            
          
           
         </div>
        );
    }

}

export default withAuth(BanUser);