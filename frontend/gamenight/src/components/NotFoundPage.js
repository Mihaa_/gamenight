import React from "react";
import Footer from './Footer';

class NotFoundPage extends React.Component {
  render() {
    return (
      <div style={{textAlign: "center", color: "white"}}>
        <h1>404 Not Found!</h1>
        <Footer />
      </div>
    );
  }
}

export default NotFoundPage;