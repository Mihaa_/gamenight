import React from 'react';
import {Alert} from 'react-bootstrap';
import { isNullOrUndefined } from 'util';

const rangeRegex = RegExp(/^(100|[1-9]?[0-9])$/);

const formValid = ({formErrors, ...rest}) => {
    let valid = true;

    Object.values(formErrors).forEach(val => val.length > 0 && (valid = false));

    const entries = Object.entries(rest);

    for (const [key, value] of entries){

        if(key != 'alertType' && key != 'alertMessage'){
            if(value ==='' && (valid = false));
        }
    }
    
    return valid;
}

class KarmaForm extends React.Component{

    constructor(props){
        super(props);

        this.state = {

            value: '',
            alertType: '',
            alertMessage: '',
            formErrors: {

                value: '',
            }
        }; 
        
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(e) {

        e.preventDefault();
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        let formErrors = this.state.formErrors;

        switch(name){

            case 'value':
                formErrors.value = rangeRegex.test(value) ? '' : 'Vrijednost mora biti između 0 i 100';
                break;

            default: break;


        }

        this.setState({formErrors, [name]: value});
    }

    handleSubmit(e) {
        e.preventDefault();

        if(formValid(this.state)){

            console.log(`The form was submitted with the following data->
            Value: ${this.state.value}
            `);

            this.setState(
                {alertType: "success",
                 alertMessage: "Karma promijenjena!"
            });
        }
        else{

            this.setState(
                {alertType: "warning",
                 alertMessage: "Neispravna vrijednost!"
            });
        }
        
    }


        

    

    render(){

        const {formErrors} = this.state;

        const alertType = this.state.alertType;
        const alertMessage = this.state.alertMessage;
        let message;

        if(alertType=="success" || alertType=="warning" || alertType=="info"){
            message = <Alert bsStyle={alertType}>{alertMessage}</Alert>
        }

        return(

            <div className="kForm">
                <form onSubmit={this.handleSubmit}>
                    <div><input className="FormField__Input" placeholder="Upišite vrijednost karme" onChange={this.handleChange} value={this.state.value} name="value" id="value" />
                    </div>
                    <br />
                    <div>
                    <div style={{display: "inlineBlock"}}><button type="submit" className="btn btn-secondary btn-md">Spremi</button></div>
                    <div style={{display: "inlineBlock"}}>{message}  </div>
                    </div>     
                </form>
                
            </div>
        );
    }


}

export default KarmaForm;