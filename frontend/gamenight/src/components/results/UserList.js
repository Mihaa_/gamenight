import React from "react";
import BanUser from "../BanUser";

class UserList extends React.Component {

    constructor(props){

        super(props);
        this.state = {

            results: [],
            error: false
            
        }
        
    }
    componentWillMount(){


        fetch('http://localhost:5000/api/korisnici')
        .then(results => {
            return results.json();
        })
        .then(data => {

            let users = data.map(u => {

                if(u.korisnickoIme!=='admin'){
                    
                    return(
                        
                        <div key={u.idKor} className="ListItem kor">
                    
                            <div className="ListItemName">
                                {u.korisnickoIme}<br />
                            </div>
    
                            <hr />
                            
                            <br />
                            
                            <div className="ListItemOpis">
                                Ime: {u.ime}<br />
                                Prezime: {u.prezime}<br />
                                Email: {u.email} <br />
                                Mobitel: {u.mobitel} <br />
    
                                Karma: {u.karma}<br />
                                
                            </div>
    
                            <br />
                        </div>
                        
                            
                    )
                }
            })
            this.setState({results: users});
            console.log("state: ", this.state.results);

        })
        .catch(error => {
            console.log("Error");   
            this.setState({error: true});
        });
    }

    render(){

        return(
            
            <div className="List">
                
                    <h1>Popis svih korisnika</h1><br />

                     <div className="ListBtn">
                        <BanUser />
                    </div>
                    {this.state.results}
                    
                    
            </div>
            



        );
    }
}

export default UserList;