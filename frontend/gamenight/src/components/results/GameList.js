import React from "react";
import {Link} from 'react-router-dom';

class GameList extends React.Component {

    constructor(props){

        super(props);
        this.state = {

            results: [],
            error: false
        }
    }

    componentWillMount(){

        fetch('http://localhost:5000/api/igre')
        .then(results => {
            return results.json();
        })
        .then(data => {

            let games = data.map(g => {
                return(
                    <div key={g.idIgre} className="ListItem">
                        <div className="ListItemName">
                            {g.naziv}
                        </div>
                        
                        <br />
                        
                        <div className="ListItemOpis">
                            {g.opis}
                        </div>

                        <br />

                        <div className="ListItemLink">
                            Link igre: <a href={g.link}>{g.link}</a>
                        </div>
                    </div>
                )
            })
            this.setState({results: games});
            console.log("state: ", this.state.results);

        })
        .catch(error => {
            console.log("Error");   
            this.setState({error: true});
        });
    }

    render(){

        return(
            
            <div className="List">
                
                    <h1>Popis svih igara</h1>
                    <div className="ListBtn">
                        <Link to="/dodaj"><button type="button" className="btn btn-secondary btn-md">
                            <img width="30px" height="30px" alt=""
                                src="https://iconsplace.com/wp-content/uploads/_icons/ff4d93/256/png/plus-2-icon-256.png"></img>
                                Dodaj igru
                        </button></Link>
                    </div>
                    
                    {this.state.results}
            </div>
            



        );
    }
}

export default GameList;