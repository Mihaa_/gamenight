import React from 'react';
import HeaderLoggedIn from './HeaderLoggedIn';

import withAuth from './auth/withAuth';
import HeaderAdmin from './HeaderAdmin';

class App extends React.PureComponent {

  render(){
    
    let admin = this.props.user.username;
    let renderAdmin = false;
    if(admin==="admin"){
        renderAdmin=true
    }
      return(
        <div className="App">
          {renderAdmin?<HeaderAdmin  history={this.props.history} user={this.props.user} /> : <HeaderLoggedIn history={this.props.history} user={this.props.user} />}
            <div className="App-header">
                <h2>Welcome {this.props.user.idUser}{this.props.user.name}</h2> {console.log(this.props.user)}
            </div>
            </div>
    );

  }
  
}

export default withAuth(App);
