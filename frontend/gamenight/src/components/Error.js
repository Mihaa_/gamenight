import React from "react";

const Error = props => {
  return (
    <h1 className="error">
      <span>Error</span>
      <p>{props.message}</p>
    </h1>
  );
};

export default Error;