import React from "react";
import Select from 'react-select';

import {Alert} from 'react-bootstrap';

class DidntAttend extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            check: undefined,
            alertMessage: '',
            alertType: ''
        }

        this.attend = this.attend.bind(this);
        this.notAttend = this.notAttend.bind(this);
        this.getOptions = this.getOptions.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event){
        //console.log(event)
        this.setState({check: event.value})
    }

    notAttend(){

        console.log(this.state.check)
        console.log(this.props.eventId)

        fetch('http://localhost:5000/prisustvo/nedolazak', {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    idKor: this.state.check,
                    idDog:this.props.eventId
                })
            })
            .then(response => {
                if(!response.ok && response.status!==400){
                    throw new Error(response.status);
                }
                else{
                   
                    return response.json();
                }
            })
            .then(response => {

                this.setState({alertMessage:"Uspjeh!", alertType:"success"});
                
            })
            .catch((e) => {
                console.log(e.message);
                this.setState({alertMessage:"Uspjeh!", alertType:"success"});
                
            });
    }

    attend(){

        console.log(this.state.check)
        console.log(this.props.eventId)

        fetch('http://localhost:5000/prisustvo', {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    idKor: this.state.check,
                    idDog:this.props.eventId
                })
            })
            .then(response => {
                if(!response.ok && response.status!==400){
                    throw new Error(response.status);
                }
                else{
                   
                    return response.json();
                }
            })
            .then(response => {

                this.setState({alertMessage:"Uspjeh!", alertType:"success"});
                
            })
            .catch((e) => {
                this.setState({alertMessage:"Uspjeh!", alertType:"success"});
                
            });




    }


    getOptions(){
        let options = []
        this.props.users.map(user => {
            if(user.korisnickoIme!=="admin"){
                options.push({value: user.idKor, label: user.korisnickoIme, key: user.idKor});
               
            }
        })
        return options;

    }

    render(){

        let message;
        const alertType = this.state.alertType;
        const alertMessage = this.state.alertMessage;

        if(alertType==="success" || alertType==="warning" || alertType==="info"){
            message = <Alert style={{width: "100px", marginLeft:"20px"}} bsStyle={alertType}>{alertMessage}</Alert>
        }

        let opt = this.getOptions();

        return(
            <div style={{margin:"auto"}}>
                {message}
                <div style={{display:"inlineBlock",width: "100px"}}>
                
                    <Select 
                        className="select" 
                        options={opt} 
                        onChange={this.handleChange} 
                        placeholder="Odaberite korisnika"
                    />
                       
                </div>   
                <div style={{display:"inlineBlock", marginLeft:"20px", marginTop:"10px"}}>
                    <button onClick={this.attend} type="button" className="btn btn-secondary btn-md">
                            Došao
                    </button>
                </div> 

                <div style={{display:"inlineBlock", marginLeft:"20px", marginTop:"10px"}}>
                    <button onClick={this.notAttend} type="button" className="btn btn-secondary btn-md">
                            Nije došao
                    </button>
                </div> 
                
         </div>
        );
    }

}

export default DidntAttend;