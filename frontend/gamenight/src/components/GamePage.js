import React from 'react';
import HeaderLoggedIn from './HeaderLoggedIn';
import GameList from './results/GameList';

import withAuth from './auth/withAuth';
import HeaderAdmin from './HeaderAdmin';

class GamePage extends React.PureComponent {


    render(){

        let admin = this.props.user.username;
        let renderAdmin = false;
        if(admin==="admin"){
            renderAdmin=true
        }

        
            return(

                <div>
                   
                   {renderAdmin?<HeaderAdmin  history={this.props.history} user={this.props.user} /> : <HeaderLoggedIn history={this.props.history} user={this.props.user} />}
                    
                    <GameList />
                </div>
            );

    }

        
}


export default withAuth(GamePage);