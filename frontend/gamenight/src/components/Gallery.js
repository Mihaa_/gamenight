import React from 'react';
import { UncontrolledCarousel } from 'reactstrap';

const items = [
    {
      src: 'https://siliconangle.com/wp-content/blogs.dir/1/files/2015/03/League-of-Legends-wallpaper.jpg',
      altText: 'Slide 1',
      caption: 'Neki magazin',
      header: 'Platforma godine!'
    },
    {
      src: 'https://images.wallpaperscraft.com/image/battlefield_4_game_ea_digital_illusions_ce_93159_1920x1080.jpg',
      altText: 'Slide 2',
      caption: 'Pero Peric',
      header: 'Nevjerojatno iskustvo!'
    },
    {
      src: 'https://img3.akspic.com/image/13686-darkness-pc_game-video_game-mountain-playstation_2-1920x1080.jpg',
      altText: 'Slide 3',
      caption: 'Anoniman korisnik',
      header: 'Zaslužuje odličnu ocjenu projekta'
    }
  ];

const Gallery = () => <UncontrolledCarousel items={items} />;

export default Gallery;