import React from 'react';
import { Link } from "react-router-dom";

import AuthService from './auth/AuthService';
import withAuth from './auth/withAuth';
const Auth = new AuthService();

class HeaderLoggedIn extends React.PureComponent {

  handleLogout(){
    Auth.logout()
    this.props.history.replace('/');
 }

  render() {
    return (
        <div className="HeaderLoggedIn">
            <header id="mainHeader" className="container-fluid">  
            <nav id="headerBar" className="navbar fixed-top navbar-dark bg-dark navbar-expand-lg">
              
                <Link to="/welcome" className="navbar-brand">
                <img src="..\icons8-nintendo-gamecube-controller-96.png" width="50" height="50" alt="" />
                GameNight
                </Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarColor01">

                <ul className="nav navbar-left mr-auto">
                  <li className="nav-item">
                  <Link to="/igre" className="nav-link"> 
                    <button type="button" className="btn btn-secondary btn-md">
                    <img width="30px" height="30px" alt=""
                    src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-game-controller-b-128.png"></img> Igre
                    </button>
                  </Link></li>
                  {/*<li className="nav-item">
                    <Link to="#" className="nav-link"> 
                      <button type="button" className="btn btn-secondary btn-md"> 
                      <img width="30px" height="30px" alt=""
                    src="https://cdn3.iconfinder.com/data/icons/watchify-v1-0-80px/80/calendar-80px-128.png"></img> Događaji
                    <span className="badge badge-pill badge-danger">12</span>
                    </button>
                    </Link>
    </li>*/}
                  <li className="nav-item">
                  <Link to="/novi-dogadjaj" className="nav-link"> 
                    <button type="button" className="btn btn-secondary btn-md">
                    <img width="30px" height="30px" alt=""
                    src="https://cdn4.iconfinder.com/data/icons/wirecons-free-vector-icons/32/add-128.png"></img> Stvori događaj
                    </button>
                  </Link></li>

                  <li className="nav-item">
                  <Link to="/korisnici" className="nav-link"> 
                    <button type="button" className="btn btn-secondary btn-md">
                    <img width="30px" height="30px" alt=""
                    src="https://iconsplace.com/wp-content/uploads/_icons/cfcfcf/256/png/contacts-icon-256.png"></img> Korisnici
                    </button>
                  </Link></li>
                </ul>

                <ul className="nav navbar-right">
                  <li className="nav-item">
                  <Link to="/profile" className="nav-link"> 
                    <button type="button" className="btn btn-secondary btn-md">
                    <img width="30px" height="30px" alt=""
                    src="https://cdn3.iconfinder.com/data/icons/user-avatars-1/512/users-11-2-128.png"></img> {this.props.user.username}</button>
                  </Link></li>
                  <li className="nav-item">
                    <Link to="#" className="nav-link"> 
                      <button type="button" className="btn btn-danger btn-md" onClick={this.handleLogout.bind(this)}> 
                      <img width="30px" height="30px" alt=""
                      src="https://cdn4.iconfinder.com/data/icons/glyphs/24/icons_exit2-128.png"></img> Odjava</button>
                    </Link>
                  </li>
                </ul>
              </div>
            </nav>
        </header>
        
        </div>
        );
    }
}

export default withAuth(HeaderLoggedIn);