import React from "react";
import { Link, NavLink } from "react-router-dom";
import AuthService from './auth/AuthService';
import {Alert} from 'react-bootstrap';

class SignInForm extends React.Component {

    constructor() {
        super();

        this.state = {
            email: '',
            password: '',
            alertMessage: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.Auth = new AuthService();
    }

    componentWillMount(){
        if(this.Auth.loggedIn())
            this.props.history.replace('/welcome');
    }

    handleChange(e) {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
          [name]: value
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        //console.log('The form was submitted with the following data:');
        //console.log(this.state);

        this.Auth.login(this.state.email,this.state.password)
            .then(res =>{
                //console.log("sada smo ulogirani");
                this.props.history.replace('/welcome');
            })
            .catch(err =>{
                this.setState({alertMessage: err.message});
            })
    }
    render() {

        const alertMessage = this.state.alertMessage;
        let message;

        if(alertMessage!==""){
            message = <Alert bsStyle="warning">{alertMessage}</Alert>
        }
      
        return (
            <div className="sform">
                <div className="PageSwitcher">
                    <NavLink to="/signin" activeClassName="PageSwitcher__Item--Active" className="PageSwitcher__Item">Prijava</NavLink>
                    <NavLink exact to="/signup" activeClassName="PageSwitcher__Item--Active" className="PageSwitcher__Item">Registracija</NavLink>
                </div>

                <div className="FormCenter">
                    <form onSubmit={this.handleSubmit} className="FormFields" noValidate>
                        <div className="FormField">
                            <label className="FormField__Label" htmlFor="email">Korisničko ime ili e-mail adresa</label>
                            <input type="text" id="email" className="FormField__Input" noValidate placeholder="Upišite korisničko ime ili e-mail adresu" name="email" value={this.state.email} onChange={this.handleChange} />
                        </div>
            
                        <div className="FormField">
                            <label className="FormField__Label" htmlFor="password">Lozinka</label>
                            <input type="password" id="password" className="FormField__Input" noValidate placeholder="Upišite lozinku" name="password" value={this.state.password} onChange={this.handleChange} />
                        </div>
            
                        <div className="FormField">
                            <button className="FormField__Button mr-20">Prijava</button> <br />
                            <Link to="/signup" className="FormField__Link">Napravi račun</Link> <br />
                            <Link to="/" className="FormField__Link">Povratak na početnu stranicu</Link>
                        </div>
                    </form>
                </div>
                {message}
               
            </div>
        );
    }
  }
  
  export default SignInForm;