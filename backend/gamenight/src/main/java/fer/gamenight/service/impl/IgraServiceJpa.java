package fer.gamenight.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import fer.gamenight.dao.IgraRepository;
import fer.gamenight.domain.Igra;
import fer.gamenight.service.IgraService;
import fer.gamenight.service.RequestDeniedException;

@Service
public class IgraServiceJpa implements IgraService{

	@Autowired
	private IgraRepository igraRepo;
	
	@Override
	public List<Igra> listIgre() {
		return igraRepo.findAll();
	}

	@Override
	public Igra createIgra(Igra igra) {
		
		Assert.notNull(igra, "Morate predati igru");
		
		if(igraRepo.countByNaziv(igra.getNaziv())>0) throw new RequestDeniedException("Igra s nazivom "+igra.getNaziv()+" vec postoji");
		return igraRepo.save(igra);
	}

	@Override
	public void deleteIgra(int id) {
		
		Assert.notNull(id, "Morate predati id igre koju zelite obrisati!");
		if(igraRepo.countByIdIgre(id) == 0) throw new RequestDeniedException("Ne mozete izbrisati igru ciji id ne postoji!");
		igraRepo.deleteById(id);
		
	}

	@Override
	public Optional<Igra> getIgraById(int id) {
		
		Assert.notNull(id, "Morate predati id igre koju zelite pretraziti!");
		
		if(igraRepo.countByIdIgre(id) == 0) throw new RequestDeniedException("Ne mozete pretraziti igru koja ne postoji!");
		return igraRepo.findById(id);
	}

	
	

}
