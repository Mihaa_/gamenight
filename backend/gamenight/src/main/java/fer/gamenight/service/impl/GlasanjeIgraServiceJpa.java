package fer.gamenight.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fer.gamenight.dao.GlasanjeIgraRepository;
import fer.gamenight.dao.KorisnikRepository;
import fer.gamenight.domain.GlasanjeDan;
import fer.gamenight.domain.GlasanjeIgra;
import fer.gamenight.domain.GlasanjeIgraKey;
import fer.gamenight.domain.Korisnik;
import fer.gamenight.service.GlasanjeIgraService;
import io.jsonwebtoken.lang.Assert;

@Service
public class GlasanjeIgraServiceJpa implements GlasanjeIgraService {
	
	@Autowired
	GlasanjeIgraRepository glasRepo;
	
	@Autowired
	KorisnikRepository korRepo;
	
	
	public void glasaj(GlasanjeIgra glas) throws Exception {
		Assert.notNull(glas, "Glas nije pravilno poslan");
		GlasanjeIgraKey kljuc=new GlasanjeIgraKey (glas.getIdKor(),glas.getIdDog(),glas.getIdIgra());
		if(glasRepo.findById(kljuc).isPresent()) throw new Exception("Već ste glasali za ovu igru"); // Ovdje se moramo vratiti jer nije valjan id od glasanja
		
		Korisnik korisnik=korRepo.findById(glas.getIdKor()).get();
		glas.setTezinaGlasa(korisnik.getKarma()*0.0025 + 1);
		//moramo izracunati tezinu glasa
		glasRepo.save(glas);
		korisnik.setKarma(korisnik.getKarma()+3);
		if(korisnik.getKarma()>100)
			korisnik.setKarma(100);
		korRepo.save(korisnik);
	}
	
	public GlasanjeIgra dohvatiGlasIgra(GlasanjeIgraKey kljuc) { //ne valja ID
		return glasRepo.findById(kljuc).get();
	}
}
