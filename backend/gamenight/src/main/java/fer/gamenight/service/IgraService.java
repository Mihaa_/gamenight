package fer.gamenight.service;
import java.util.List;
import java.util.Optional;

import fer.gamenight.domain.*;

public interface IgraService {

	List<Igra> listIgre();
	
	Igra createIgra (Igra igra);
	
	void deleteIgra(int id);
	
	Optional<Igra> getIgraById(int id);

}
