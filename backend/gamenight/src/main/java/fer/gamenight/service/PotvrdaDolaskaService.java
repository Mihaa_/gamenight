package fer.gamenight.service;

import fer.gamenight.domain.KorDogKey;
import fer.gamenight.domain.PotvrdaDolaska;
import java.util.List;
import fer.gamenight.domain.Korisnik;

public interface PotvrdaDolaskaService {
	
	public PotvrdaDolaska dohvatiDolazak(KorDogKey kljuc);  //ID ne valja
	public void spremiDolazak(PotvrdaDolaska potvrdaDolaska);
	List<Korisnik> dohvatiSveKojiSuPotvrdili(int idDog);

}
