package fer.gamenight.service;

import fer.gamenight.domain.UdaljeniKorisnik;

public interface UdaljeniKorisnikService {

	public void UdaljavanjeKorisnika(UdaljeniKorisnik udaljeni);
	public void BrisanjeIzUdaljenih(int idKor);
	public void ukidanjeZabrane();
}
