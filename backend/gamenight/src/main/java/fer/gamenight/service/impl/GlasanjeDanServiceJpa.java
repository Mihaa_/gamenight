package fer.gamenight.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fer.gamenight.dao.GlasanjeDanRepository;
import fer.gamenight.dao.KorisnikRepository;
import fer.gamenight.domain.GlasanjeDan;
import fer.gamenight.domain.GlasanjeDanKey;
import fer.gamenight.domain.Korisnik;
import fer.gamenight.service.GlasanjeDanService;
import io.jsonwebtoken.lang.Assert;
@Service
public class GlasanjeDanServiceJpa implements GlasanjeDanService {
	
	@Autowired
	GlasanjeDanRepository glasanjeDanRepo;
	@Autowired
	KorisnikRepository korisnikRepo;
	
	@Override
	public void glasaj(GlasanjeDan glasanjeDan) throws Exception {
		Assert.notNull(glasanjeDan,"Glas nije pravilno poslan");
		GlasanjeDanKey kljuc= new GlasanjeDanKey(glasanjeDan.getIdDog(),glasanjeDan.getIdKor(),glasanjeDan.getDan());
		if(glasanjeDanRepo.findById(kljuc).isPresent()) //*Moramo popraviti ID*//
			throw new Exception("Već ste glasali za ovaj dan");
		
		Korisnik pom=korisnikRepo.findById(glasanjeDan.getIdKor()).get(); //*tražimo korisnika radi računanja karme*//
		glasanjeDan.setTezinaGlasa(1+0.0025*pom.getKarma());
		glasanjeDanRepo.save(glasanjeDan);
		pom.setKarma(pom.getKarma()+3);
	}
	
	public GlasanjeDan dohvatiGlasDan(GlasanjeDanKey kljuc) { //ne valja id
		return glasanjeDanRepo.findById(kljuc).get();
	}
}