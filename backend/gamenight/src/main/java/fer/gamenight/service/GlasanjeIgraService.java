package fer.gamenight.service;

import org.springframework.stereotype.Service;

import fer.gamenight.domain.GlasanjeIgra;

public interface GlasanjeIgraService {
	
	public void glasaj(GlasanjeIgra glas) throws Exception;
	
}
