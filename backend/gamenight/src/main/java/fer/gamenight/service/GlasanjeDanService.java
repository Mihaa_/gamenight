package fer.gamenight.service;

import org.springframework.stereotype.Service;

import fer.gamenight.domain.GlasanjeDan;

public interface GlasanjeDanService {
	
	public void glasaj(GlasanjeDan glasanjeDan) throws Exception;
	
}
