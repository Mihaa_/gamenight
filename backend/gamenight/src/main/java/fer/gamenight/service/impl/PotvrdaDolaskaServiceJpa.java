package fer.gamenight.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fer.gamenight.dao.KorisnikRepository;
import fer.gamenight.dao.PotvrdaDolaskaRepository;
import fer.gamenight.domain.KorDogKey;
import fer.gamenight.domain.PotvrdaDolaska;
import fer.gamenight.service.PotvrdaDolaskaService;
import fer.gamenight.domain.Korisnik;

import java.util.ArrayList;
import java.util.List;
@Service
public class PotvrdaDolaskaServiceJpa implements PotvrdaDolaskaService {

	@Autowired
	PotvrdaDolaskaRepository potvrdaDolaskaRepo;
	
	@Autowired
	KorisnikRepository korisnikRepo;
		
	@Override
	public PotvrdaDolaska dohvatiDolazak(KorDogKey kljuc) { //*ne valja id
		return potvrdaDolaskaRepo.findById(kljuc).get(); 
	}

	@Override
	public void spremiDolazak(PotvrdaDolaska potvrdaDolaska) {
		potvrdaDolaskaRepo.save(potvrdaDolaska);
	}
	
	@Override
	public List<Korisnik> dohvatiSveKojiSuPotvrdili(int idDog) {
		List<PotvrdaDolaska> pom= potvrdaDolaskaRepo.findAll();
		List<PotvrdaDolaska> listaPotvrdaZaDogadaj= new ArrayList<>();
		for(PotvrdaDolaska y: pom) { //ubacivanje potvrda za određeni
			if (y.getIdDog()==idDog) {
				listaPotvrdaZaDogadaj.add(y);
			}		
		}
		List<Korisnik> lista= new ArrayList<>();
		for(PotvrdaDolaska x: listaPotvrdaZaDogadaj) {
			lista.add(korisnikRepo.findByIdKor(x.getIdKor()).get());
		}
		return lista;
	}

}
