package fer.gamenight.service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import fer.gamenight.domain.Dogadjaj;
import fer.gamenight.domain.Igra;
import fer.gamenight.domain.KorDogKey;
import fer.gamenight.domain.KorisnikDogadjaj;

public interface DogadjajService {
	
	
	List<Dogadjaj> pregledJavnihDogadjaja();
	List<Dogadjaj> pregledSvihDogadjaja(int id); // id je id korisnika
	void kreirajPrivatniDogadjaj(Dogadjaj dogadjaj) throws Exception;
	void kreirajJavniDogadjaj(Dogadjaj dogadjaj) throws Exception;
	Dogadjaj dohvatiDogadaj(int idDog, int id) throws Exception;
	List<Dogadjaj> pregledPrivatnihDogadjaja(int id);
	List<Dogadjaj> pregledBasSvihDogadjaja();
	void DodjelaKarmeZaPrisustvo(KorDogKey kljuc);
	void OduzimanjeKarmeZaNeGlasanje(KorDogKey kljuc);
	void posaljiPozivnicu(KorisnikDogadjaj korDog);
	void zavrsetakGlasanja();
	void zavrsetakDogadaja();
	Instant prebrojGlasDan(int idDog);
	Igra prebrojGlasIgra(int idDog);
	Optional<Dogadjaj> findDogById(int id);
	Optional<Dogadjaj> findByNaziv(String naziv);

}
