package fer.gamenight.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import fer.gamenight.dao.KorisnikRepository;
import fer.gamenight.domain.Korisnik;
import fer.gamenight.service.KorisnikService;
import fer.gamenight.service.RequestDeniedException;

@Service
public class KorisnikServiceJpa implements KorisnikService{

	@Autowired
	private KorisnikRepository korisnikRepo;

	@Override
	public List<Korisnik> listKorisnik() {
		return korisnikRepo.findAll();
	}

	@Override
	public Korisnik createKorisnik(Korisnik korisnik) {
		
		Assert.notNull(korisnik, "Morate predati korisnika");
		
		if(korisnikRepo.countByKorisnickoIme(korisnik.getKorisnickoIme()) > 0) throw new RequestDeniedException("Korisnik s korisnickim imenom "+korisnik.getKorisnickoIme()+" vec postoji");
		return korisnikRepo.save(korisnik);
	}

	@Override
	public void deleteKorisnik(int id) {
		
		Assert.notNull(id, "Morate predati id korisnika kojeg zelite obrisati!");
		if(korisnikRepo.countByIdKor(id) == 0) throw new RequestDeniedException("Ne mozete izbrisati korisnika ciji id ne postoji!");
		korisnikRepo.deleteById(id);
		
	}

	@Override
	public Optional<Korisnik> getKorisnikByKorisnickoIme(String korIme) {
		
		Assert.notNull(korIme, "Morate predati korisnicko ime korisnika kojeg zelite pretraziti!");
		
		if(korisnikRepo.countByKorisnickoIme(korIme) == 0) throw new RequestDeniedException("Ne mozete pretraziti korisnika koji ne postoji!");
		return korisnikRepo.findByKorisnickoIme(korIme);
	}

	@Override
	public Optional<Korisnik> getKorisnikByIdKor(int idKor) {
		
		Assert.notNull(idKor, "Morate predati id korisnika kojeg zelite pretraziti!");
		if(korisnikRepo.countByIdKor(idKor) == 0) throw new RequestDeniedException("Ne mozete pretraziti korisnika koji ne postoji!");
		return korisnikRepo.findByIdKor(idKor);
	}
	
	

}
