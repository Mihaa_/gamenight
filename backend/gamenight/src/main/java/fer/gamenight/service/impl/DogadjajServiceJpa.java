package fer.gamenight.service.impl;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

import fer.gamenight.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.standard.InstantFormatter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import fer.gamenight.dao.DogadjajRepository;
import fer.gamenight.dao.GlasanjeDanRepository;
import fer.gamenight.dao.GlasanjeIgraRepository;
import fer.gamenight.dao.IgraRepository;
import fer.gamenight.dao.KorisnikDogadjajRepository;
import fer.gamenight.dao.KorisnikRepository;
import fer.gamenight.dao.PotvrdaDolaskaRepository;
import fer.gamenight.dao.PrisustvoRepository;
import fer.gamenight.domain.Dogadjaj;
import fer.gamenight.domain.GlasanjeDan;
import fer.gamenight.domain.GlasanjeIgra;
import fer.gamenight.domain.Igra;
import fer.gamenight.domain.KorDogKey;
import fer.gamenight.domain.Korisnik;
import fer.gamenight.domain.KorisnikDogadjaj;
import fer.gamenight.service.DogadjajService;

@Service
public class DogadjajServiceJpa implements DogadjajService {
	
	@Autowired
	KorisnikRepository KorisnikRepo;
	
	@Autowired
	IgraRepository igraRepo;
	
	@Autowired
	DogadjajRepository dogadjajRepo;
	
	@Autowired
	KorisnikDogadjajRepository korDogRepo;
	
	@Autowired
	PotvrdaDolaskaRepository potvrdaDolaskaRepo;
	
	@Autowired
	PrisustvoRepository prisustvoRepo;
	
	@Autowired
	GlasanjeDanRepository glasanjeDanRepo;
	
	@Autowired
	GlasanjeIgraRepository glasanjeIgraRepo;

	@Override
	public Optional<Dogadjaj> findDogById(int id) {

		Assert.notNull(id, "Morate predati id igre koju zelite pretraziti!");

		if(dogadjajRepo.findById(id) == null) throw new RequestDeniedException("Ne mozete pretraziti igru koja ne postoji!");
		return dogadjajRepo.findById(id);
	}

	@Override
	public Optional<Dogadjaj> findByNaziv(String naziv) {

		Assert.notNull(naziv, "Morate predati naziv dogadjaja koju zelite pretraziti!");

		if(dogadjajRepo.findByNaziv(naziv) == null) throw new RequestDeniedException("Ne mozete pretraziti igru koja ne postoji!");
		return dogadjajRepo.findByNaziv(naziv);
	}
	
	@Override
	public List<Dogadjaj> pregledPrivatnihDogadjaja(int id /*ovo je id od prijavljenog korisnika koji salje zahjev*/) {
		
		
		List<Dogadjaj> lista=new ArrayList<>();
		List<KorisnikDogadjaj> listaKorDog=korDogRepo.findAll();
		for(KorisnikDogadjaj x: listaKorDog) {
			if(x.getIdKor()==id)
				lista.add(dogadjajRepo.findById(x.getIdDog()).get());
		}
		return lista;
	}

	@Override
	public List<Dogadjaj> pregledJavnihDogadjaja() {
		List<Dogadjaj> pomLista=dogadjajRepo.findAll();
		List<Dogadjaj> lista=new ArrayList<>();
		for(Dogadjaj x: pomLista) {
			if(!x.isPrivatni())
				lista.add(x);
		}
		return lista;
	}
	
	@Override
	public List<Dogadjaj> pregledBasSvihDogadjaja() {
		List<Dogadjaj> pomLista=dogadjajRepo.findAll();
		List<Dogadjaj> lista=new ArrayList<>();
		for(Dogadjaj x: pomLista) {
				lista.add(x);
		}
		return lista;
	}


	@Override
	public void kreirajPrivatniDogadjaj(Dogadjaj dogadjaj) throws Exception {
		
		Assert.notNull(dogadjaj, "Morate predati dogadjaj");
		if(dogadjajRepo.findById(dogadjaj.getIdDog()).isPresent())
			throw new Exception("Uneseni dogadjaj vec postoji");
		dogadjaj.setPrivatni(true);
		dogadjajRepo.save(dogadjaj);
		
		
	}
	
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Override
	public void kreirajJavniDogadjaj(Dogadjaj dogadjaj) throws Exception {
		
		Assert.notNull(dogadjaj, "Morate predati dogadjaj");
		if(dogadjajRepo.findById(dogadjaj.getIdDog()).isPresent())
				throw new Exception("Uneseni dogadjaj vec postoji");
		dogadjaj.setPrivatni(false);
		dogadjajRepo.save(dogadjaj);
		
	}

	@Override
	public Dogadjaj dohvatiDogadaj(int idDog,int id/*ovo je id trenutnog korisnika koji salje zahtjev*/) throws Exception {
		
		
		Assert.notNull(id, "Morate predati id");
		Assert.notNull(idDog, "Morate unijeti id dogadjaja");
		
		List<KorisnikDogadjaj> listaKorDog=korDogRepo.findAll();
		for(KorisnikDogadjaj x: listaKorDog) {
			if(x.getIdKor()==id && x.getIdDog()==idDog)
				return dogadjajRepo.findById(idDog).get();
		}
		
		throw new Exception("Nemate prava pristupa zeljenom dogadaju");
	}

	@Override
	public void DodjelaKarmeZaPrisustvo(KorDogKey kljuc) { //Ova funkcija se poziva nakon što admin unese prisustvo
		Korisnik korisnik= KorisnikRepo.findById(kljuc.getIdKor()).get();
		double pomKarma=korisnik.getKarma();
		double dodatak= (100-pomKarma)*0.25;
		double odbitak=(-100-pomKarma)*0.25;
		
		if(prisustvoRepo.findById(kljuc).isPresent() && potvrdaDolaskaRepo.findById(kljuc).isPresent()) //ID NE VALJA
			korisnik.setKarma(korisnik.getKarma()+dodatak);
		else if(!prisustvoRepo.findById(kljuc).isPresent() && potvrdaDolaskaRepo.findById(kljuc).isPresent())
				korisnik.setKarma(korisnik.getKarma()+odbitak);
		
		if(korisnik.getKarma()>100) 
			korisnik.setKarma(100);
		if(korisnik.getKarma()<-100)
			korisnik.setKarma(-100);
		KorisnikRepo.save(korisnik);
	}
	@Override
	public void OduzimanjeKarmeZaNeGlasanje(KorDogKey kljuc) { //za pozitivnu karmu oduzimamo,za negativnu dodajemo
		Korisnik korisnik= KorisnikRepo.findById(kljuc.getIdKor()).get();
		
		if(korDogRepo.findById(kljuc).isPresent() && !glasanjeDanRepo.findByIdKorAndIdDog(kljuc.getIdKor(),kljuc.getIdDog()).isPresent()) {
			if(korisnik.getKarma()>0)
				korisnik.setKarma(korisnik.getKarma()-0.1*korisnik.getKarma());
			else
				korisnik.setKarma(korisnik.getKarma()+0.1*korisnik.getKarma());
		}
			if (korisnik.getKarma()<-100)
				korisnik.setKarma(-100);
		
			KorisnikRepo.save(korisnik);
	}

	@Override
	public void posaljiPozivnicu(KorisnikDogadjaj korDog) {
		korDogRepo.save(korDog);
	}
	
	@Scheduled(fixedRate = 30000, initialDelay = 30000)
	@Override
	public void zavrsetakDogadaja() {
		List<Dogadjaj> pom = dogadjajRepo.findAll();
		for(Dogadjaj x:pom) {
			if(x.isIzglasan() && !x.isZavrsen()) {
				int vrijeme=x.getDatumIsteka().truncatedTo(ChronoUnit.MINUTES).
						compareTo(Instant.now().truncatedTo(ChronoUnit.MINUTES));
				if(vrijeme==-1) {
					x.setZavrsen(true);
					dogadjajRepo.save(x);
				}
			}
		}
	}
	
	@Scheduled(fixedRate = 30000)
	@Override
	public void zavrsetakGlasanja() {
		List<Dogadjaj> pom = dogadjajRepo.findAll();
		for(Dogadjaj x:pom) {
			if(!x.isIzglasan()) {
				int vrijeme=(x.getDatumIsteka().minusSeconds(604800).truncatedTo(ChronoUnit.MINUTES))
						.compareTo(Instant.now().truncatedTo(ChronoUnit.MINUTES));
				if(vrijeme==-1) {
					List<Korisnik> korisnici = KorisnikRepo.findAll();
					for(Korisnik kor:korisnici) {
						KorDogKey kljuc=new KorDogKey(x.getIdDog(), kor.getIdKor());
						OduzimanjeKarmeZaNeGlasanje(kljuc);
						
					}
					
					x.setIzglasan(true);

					x.setIgra(prebrojGlasIgra(x.getIdDog()));
					x.setDatumIsteka(prebrojGlasDan(x.getIdDog())); ///postavljanje izglasanog datuma,kao datum održavanja
					dogadjajRepo.save(x);
				}	
			}
		}
		
	}

	@Override
	public Instant prebrojGlasDan(int idDog) {
		
		List<GlasanjeDan> pom=glasanjeDanRepo.findAll();
		List<GlasanjeDan> glasovi= new ArrayList<>();
		
		for(GlasanjeDan x: pom) {
			if(x.getIdDog()==idDog)
				glasovi.add(x); //u listi glasovi se nalaze glasovi samo za trazeni dogadaj
		}
		
		
		//--------------------------------//Zbrajanje glasova za svaki dan. Kljuc je dan, vrijednost zbroj glasova
		Map<Instant, Double> mapa=new LinkedHashMap<>();
		for(GlasanjeDan x: glasovi) {
			if(mapa.containsKey(x.getDan())) {
				mapa.put(x.getDan(), mapa.get(x.getDan())+x.getTezinaGlasa());
			}else {
				mapa.put(x.getDan(), x.getTezinaGlasa());
			}
		}
		
		//--------------------------------Odredivanje dana s najvise glasova
		Instant idDan =null;
		Double max=0.0;
		for(GlasanjeDan x: glasovi) {
			if(mapa.get(x.getDan())>max) {
				idDan=x.getDan();
				max=mapa.get(idDan);
			}
		}
		if(idDan==null) {
			return findDogById(idDog).get().getDatumIsteka();
		}else
			return idDan;
	}



	@Override
	public Igra prebrojGlasIgra(int idDog) {

		List<Igra> listaIgara=igraRepo.findAll();
		List<GlasanjeIgra> pom;
		Map<Integer,Double> mapa=new LinkedHashMap<>();
		for(Igra igra:listaIgara) {
			pom=glasanjeIgraRepo.findAllByIdIgra(igra.getIdIgre()); //Dodaj sva glasanja za određenu igru
			double zbroj=0;
			for(GlasanjeIgra x:pom) {
				zbroj+=x.getTezinaGlasa();	//Zbroji im težine
			}
			mapa.put(igra.getIdIgre(),zbroj);
		}
		
		double maxZbroj=mapa.get(listaIgara.get(0).getIdIgre());		//Traženje maximalnog zbroja
		int maxId=listaIgara.get(0).getIdIgre();
		
		for(Map.Entry<Integer, Double> x : mapa.entrySet()) {
			if (x.getValue()>maxZbroj) {
				maxZbroj=x.getValue();
				maxId=x.getKey();
			}
		}
		return igraRepo.findById(maxId).get();
	}

	@Override
	public List<Dogadjaj> pregledSvihDogadjaja(int id) { //id korisnika
		List<Dogadjaj> pom=new ArrayList<>();
		pom.addAll(pregledPrivatnihDogadjaja(id));
		pom.addAll(pregledJavnihDogadjaja());
		return pom;
	}

	

	
}
