package fer.gamenight.service;

import org.springframework.stereotype.Service;

import fer.gamenight.domain.KorDogKey;
import fer.gamenight.domain.Prisustvo;

public interface PrisustvoService {
	void spremiPrisustvo(Prisustvo prisustvo);
	Prisustvo dohvatiPrisustvo(KorDogKey kljuc);
	void unesiNeprisustvo(Prisustvo prisustvo);
}
