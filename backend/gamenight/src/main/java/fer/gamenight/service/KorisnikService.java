package fer.gamenight.service;

import java.util.List;
import java.util.Optional;

import fer.gamenight.domain.*;

public interface KorisnikService {
	
	List<Korisnik> listKorisnik();
	
	Korisnik createKorisnik(Korisnik korisnik);
	
	void deleteKorisnik(int id);
	
	Optional<Korisnik> getKorisnikByKorisnickoIme(String korIme);
	
	Optional<Korisnik> getKorisnikByIdKor(int idKor);
}
