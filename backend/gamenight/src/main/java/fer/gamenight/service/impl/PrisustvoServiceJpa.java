package fer.gamenight.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fer.gamenight.dao.PrisustvoRepository;
import fer.gamenight.domain.KorDogKey;
import fer.gamenight.domain.Prisustvo;
import fer.gamenight.service.PrisustvoService;

@Service
public class PrisustvoServiceJpa implements PrisustvoService {
	
	@Autowired
	PrisustvoRepository prisustRepo;
	
	@Autowired
	DogadjajServiceJpa dogService;

	@Override
	public Prisustvo dohvatiPrisustvo(KorDogKey kljuc) { //ne valja ID
		return prisustRepo.findById(kljuc).get();
	}

	@Override
	public void spremiPrisustvo(Prisustvo prisustvo) {
		prisustRepo.save(prisustvo);
		KorDogKey kljuc= new KorDogKey(prisustvo.getIdDog(),prisustvo.getIdKor());
		dogService.DodjelaKarmeZaPrisustvo(kljuc);
	}
	
	@Override
	public void unesiNeprisustvo(Prisustvo prisustvo) {
		KorDogKey kljuc= new KorDogKey(prisustvo.getIdDog(),prisustvo.getIdKor());
		dogService.DodjelaKarmeZaPrisustvo(kljuc);
	}
	
	
}
