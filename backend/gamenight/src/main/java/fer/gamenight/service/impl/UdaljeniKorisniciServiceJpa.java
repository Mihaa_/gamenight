package fer.gamenight.service.impl;


import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import fer.gamenight.dao.UdaljeniKorisnikRepository;
import fer.gamenight.domain.UdaljeniKorisnik;
import fer.gamenight.service.UdaljeniKorisnikService;

@Service
public class UdaljeniKorisniciServiceJpa implements UdaljeniKorisnikService {
	@Autowired
	UdaljeniKorisnikRepository udaljeniRepo;
	
	public void UdaljavanjeKorisnika(UdaljeniKorisnik udaljeni) {
		udaljeniRepo.save(udaljeni);
	}
	//Treba napravit dretvu za vrijeme koja će brisati sve kojima je vrijeme kazne isteklo
	public void BrisanjeIzUdaljenih(int idKor) {
		udaljeniRepo.deleteById(idKor);
	}
	
	@Scheduled(fixedRate=300000, initialDelay=300000)
	public void ukidanjeZabrane() {
		List<UdaljeniKorisnik> udaljeni=udaljeniRepo.findAll();
		for(UdaljeniKorisnik x: udaljeni) {
			if(x.getVrijemeKazne().truncatedTo(ChronoUnit.MINUTES).compareTo(Instant.now().truncatedTo(ChronoUnit.MINUTES))<0) {
				udaljeniRepo.deleteById(x.getIdKor());
			}
		}
	}
	
	
}