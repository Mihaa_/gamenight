package fer.gamenight.rest;

import fer.gamenight.dao.PotvrdaDolaskaRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fer.gamenight.domain.KorDogKey;
import fer.gamenight.domain.Korisnik;
import fer.gamenight.domain.PotvrdaDolaska;
import fer.gamenight.service.PotvrdaDolaskaService;

@RestController
@RequestMapping("/dolazak")
public class PotvrdaDolaskaController {
	@Autowired
	PotvrdaDolaskaService PDservice;

	@Autowired
	PotvrdaDolaskaRepository potvrdaDolaskaRepo;
	
	@GetMapping("/{id}")
	public List<Korisnik> dohvatiSveKojiSuPotvrdili(@PathVariable int id){
		return PDservice.dohvatiSveKojiSuPotvrdili(id);
	}

	@PostMapping("")
	public void spremiDolazak(@RequestBody PotvrdaDolaska potvrdaDolaska) {
		PDservice.spremiDolazak(potvrdaDolaska);
	}

}
