package fer.gamenight.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fer.gamenight.domain.GlasanjeIgra;
import fer.gamenight.service.impl.GlasanjeIgraServiceJpa;

@RestController
@RequestMapping("/dogadjaj/glasanje")
public class GlasanjeIgraController {
	
	@Autowired
	GlasanjeIgraServiceJpa glasService;
	
	@PostMapping("/igra")
	public void glasaj(@RequestBody GlasanjeIgra glas) throws Exception {
		glasService.glasaj(glas);
	}

}
