package fer.gamenight.rest;

import java.util.List;
import java.util.Optional;

import fer.gamenight.domain.Igra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fer.gamenight.domain.Dogadjaj;
import fer.gamenight.domain.KorisnikDogadjaj;
import fer.gamenight.service.DogadjajService;
@RestController
@RequestMapping("/dogadjaj")
public class DogadjajController {

	@Autowired
	DogadjajService servis;

	@GetMapping("/privatni/{id}")
	public List<Dogadjaj> pregledPrivatnihDogadjaja(@PathVariable int id /*korisnikov ID*/){
		return servis.pregledPrivatnihDogadjaja(id);
	}

	@GetMapping("/javni")
	public List<Dogadjaj> pregledJavnihDogadjaja(){
		return servis.pregledJavnihDogadjaja();
	}

	@GetMapping("/{id}")
	public Optional<Dogadjaj> findDogById(@PathVariable int id) {

		return servis.findDogById(id);
	}

	@GetMapping("/naziv/{naziv}")
	public Optional<Dogadjaj> findByNaziv(@PathVariable String naziv) {
		return servis.findByNaziv(naziv);
	}

	@GetMapping("")
	public List<Dogadjaj> pregledSvihDogadjaja(int id){
		return servis.pregledSvihDogadjaja(id);
	}
	
	@GetMapping("/svi")
	public List<Dogadjaj> pregledBasSvihDogadjaja(){
		return servis.pregledBasSvihDogadjaja();
		
	}

	@PostMapping("/privatni")
	public void kreirajPrivatniDogadjaj(@RequestBody Dogadjaj dogadjaj) throws Exception {
		if(!dogadjaj.isPrivatni()) throw new Exception ("Događaj koji pokušavate kreirati mora biti privatan");
		servis.kreirajPrivatniDogadjaj(dogadjaj);
	}

	 @PostMapping("/privatni/pozivnice")
	 public void posaljiPozivnicu(@RequestBody KorisnikDogadjaj korDog){
		 servis.posaljiPozivnicu(korDog);
	 }

	@PostMapping("/javni")
	public void kreirajJavniDogadjaj(@RequestBody Dogadjaj dogadjaj) throws Exception{
		if(dogadjaj.isPrivatni()) throw new Exception ("Događaj koji pokušavate kreirati mora biti javan");
		servis.kreirajJavniDogadjaj(dogadjaj);
	}
}
