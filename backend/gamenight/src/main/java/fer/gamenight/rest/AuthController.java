package fer.gamenight.rest;

import fer.gamenight.exception.*;
import fer.gamenight.domain.*;
import fer.gamenight.payload.*;
import fer.gamenight.dao.*;
import fer.gamenight.security.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

	@Autowired
	UdaljeniKorisnikRepository udaljeniRepo;
	
	@Autowired
    AuthenticationManager authenticationManager;

	@Autowired
	KorisnikRepository korisnikRepo;

	@Autowired
	RoleRepository roleRepo;

	@Autowired
    PasswordEncoder passwordEncoder;

	@Autowired
    JwtTokenProvider tokenProvider;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest){
		
		Korisnik noviKorisnik=korisnikRepo.findByKorisnickoImeOrEmail(loginRequest.getUsernameOrEmail(), loginRequest.getUsernameOrEmail()).get();
		if(udaljeniRepo.findById(noviKorisnik.getIdKor()).isPresent()) {
			return new ResponseEntity(new ApiResponse(false, "Banirani ste! Vrijeme isteka kazne je: "
															+ udaljeniRepo.findById(noviKorisnik.getIdKor()).get().getVrijemeKazne().toString()),
                    HttpStatus.FORBIDDEN);
		}
		
		Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );
		
		
		
		SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {

		if(korisnikRepo.existsByKorisnickoIme(signUpRequest.getKorisnickoIme())) {
			return new ResponseEntity(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
		}

		if(korisnikRepo.existsByEmail(signUpRequest.getEmail())) {

			return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
		}

		// Creating user's account

		Korisnik korisnik = new Korisnik (
				signUpRequest.getIme(), signUpRequest.getPrezime(),
				signUpRequest.getKorisnickoIme(), signUpRequest.getEmail(),
				signUpRequest.getMobitel(), signUpRequest.getLozinka()
				);

		korisnik.setLozinka(passwordEncoder.encode(korisnik.getLozinka()));

		Role korisnikUloga = roleRepo.findByName(RoleName.ROLE_USER)
				.orElseThrow(()-> new AppException("User role not set"));

		korisnik.setRoles(Collections.singleton(korisnikUloga));

		Korisnik rezultat = korisnikRepo.save(korisnik);

		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/api/korisnici/{korisnickoIme}")
				.buildAndExpand(rezultat.getKorisnickoIme()).toUri();

		return ResponseEntity.created(location)
							 .body(new ApiResponse(true, "Korisnik uspjesno registriran"));
	}
}
