package fer.gamenight.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fer.gamenight.domain.UdaljeniKorisnik;
import fer.gamenight.service.UdaljeniKorisnikService;

@RestController
@RequestMapping ("/udaljavanje")
public class UdaljeniKorisnikController {
	
	@Autowired
	UdaljeniKorisnikService service;

	@PostMapping("")
	public void UdaljavanjeKorisnika(@RequestBody UdaljeniKorisnik udaljeni) {
		service.UdaljavanjeKorisnika(udaljeni);
	}
	
	@DeleteMapping("")
	public void BrisanjeIzUdaljenih(int idKor) {
		service.BrisanjeIzUdaljenih(idKor);
	}
}
