package fer.gamenight.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fer.gamenight.domain.GlasanjeDan;
import fer.gamenight.service.GlasanjeDanService;

@RestController
@RequestMapping("/dogadjaj/glasanje")
public class GlasanjeDanController {
	
	@Autowired
	GlasanjeDanService glasanjeDanService;
	
	@PostMapping("/dan")
	public void glasanjeDan(@RequestBody GlasanjeDan glasanjeDan) throws Exception {
	glasanjeDanService.glasaj(glasanjeDan);
	}
	
	
}
