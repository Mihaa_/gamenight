package fer.gamenight.rest;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fer.gamenight.domain.*;
import fer.gamenight.service.IgraService;

@RestController
@RequestMapping("/api/igre")
public class IgraController {

	@Autowired
	private IgraService igraService;
	
	@GetMapping("")
	public List<Igra> listIgre() {
		
		return igraService.listIgre();
	}
	
	@PostMapping("")
	public Igra createIgra(@RequestBody Igra igra) {
		return igraService.createIgra(igra);
	}
	
	@DeleteMapping("/{id}")
	public void deleteIgra (@PathVariable int id) {
		
		igraService.deleteIgra(id);
	}
	
	@GetMapping("/{id}")
	public Optional<Igra> getIgraById(@PathVariable int id) {
		
		return igraService.getIgraById(id);
	}
	
}
