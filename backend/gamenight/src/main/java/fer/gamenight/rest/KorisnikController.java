package fer.gamenight.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fer.gamenight.domain.Korisnik;
import fer.gamenight.service.KorisnikService;

@RestController
@RequestMapping("/api/korisnici")
public class KorisnikController {

	@Autowired
	private KorisnikService korisnikService;
	
	@GetMapping("")
	public List<Korisnik> listKorisnik(){
		
		return korisnikService.listKorisnik();
	}
	
	@PostMapping("")
	public Korisnik createKorisnik(@RequestBody Korisnik korisnik) {
		
		return korisnikService.createKorisnik(korisnik);
	}
	
	@DeleteMapping("/{id}")
	public void deleteKorisnik(@PathVariable int id) {
		
		korisnikService.deleteKorisnik(id);
	}
	
	@GetMapping("/{idKor}")
	public Optional<Korisnik> getKorisnikById(@PathVariable int idKor){
		
		return korisnikService.getKorisnikByIdKor(idKor);
	}

}
