package fer.gamenight.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fer.gamenight.domain.KorDogKey;
import fer.gamenight.domain.Prisustvo;
import fer.gamenight.service.PrisustvoService;


@RestController
@RequestMapping("/prisustvo")
public class PrisustvoController {

	@Autowired
	PrisustvoService servis;
	
	@GetMapping("")
	Prisustvo dohvatiPrisustvo(@RequestBody KorDogKey kljuc) {
		return servis.dohvatiPrisustvo(kljuc);
	}
	
	@PostMapping("")
	void spremiPrisustvo(@RequestBody Prisustvo prisustvo) {
		servis.spremiPrisustvo(prisustvo);
	}
	
	@PostMapping("/nedolazak")
	void spremiNeprisustvo(@RequestBody Prisustvo prisustvo) {
		servis.unesiNeprisustvo(prisustvo);
	}
	

}
