package fer.gamenight.security;

import fer.gamenight.domain.Korisnik;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class KorisnikPrincipal implements UserDetails{

	private int idKor;
	private String ime;
	private String prezime;
	private String korisnickoIme;
	
	@JsonIgnore
	private String email;
	
	@JsonIgnore
	private String lozinka;
	
	private Collection<? extends GrantedAuthority> authorities;
	
	public KorisnikPrincipal(int idKor, String ime, String prezime, String korisnickoIme, String email, String lozinka, Collection<? extends GrantedAuthority> authorities) {
		
		this.idKor = idKor;
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoIme = korisnickoIme;
		this.email = email;
		this.lozinka = lozinka;
		this.authorities = authorities;
	}

	public static KorisnikPrincipal create (Korisnik korisnik) {
		
		List<GrantedAuthority> authorities = korisnik.getRoles().stream().map(role -> 
				new SimpleGrantedAuthority(role.getName().name())).collect(Collectors.toList());
		
		return new KorisnikPrincipal(
				korisnik.getIdKor(), 
				korisnik.getIme(), 
				korisnik.getPrezime(), 
				korisnik.getKorisnickoIme(), 
				korisnik.getEmail(), 
				korisnik.getLozinka(),
				authorities);
	}
	
	public int getIdKor() {
        return idKor;
    }

    public String getIme() {
        return ime;
    }
    
    public String getPrezime() {
        return prezime;
    }

    public String getEmail() {
        return email;
    }
    
    @Override
	public String getPassword() {
		return lozinka;
	}

	@Override
	public String getUsername() {
		return korisnickoIme;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KorisnikPrincipal that = (KorisnikPrincipal) o;
        return Objects.equals(idKor, that.idKor);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idKor);
    }

}
