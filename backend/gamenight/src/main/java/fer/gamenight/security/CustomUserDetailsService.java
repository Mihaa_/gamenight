package fer.gamenight.security;

import fer.gamenight.domain.Korisnik;
import fer.gamenight.dao.KorisnikRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService{

	@Autowired
	KorisnikRepository korisnikRepo;
	

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
		
		Korisnik korisnik = korisnikRepo.findByKorisnickoImeOrEmail(usernameOrEmail, usernameOrEmail)
				.orElseThrow(()-> new UsernameNotFoundException("User not found with username or email : " + usernameOrEmail));
	
		return KorisnikPrincipal.create(korisnik);
	}
	
	// This method is used by JWTAuthenticationFilter
	
	@Transactional
    public UserDetails loadUserById(int id) {
        Korisnik korisnik = korisnikRepo.findByIdKor(id).orElseThrow(
            () -> new UsernameNotFoundException("User not found with id : " + id)
        );

        return KorisnikPrincipal.create(korisnik);
    }

}
