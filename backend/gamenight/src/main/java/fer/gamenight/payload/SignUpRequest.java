package fer.gamenight.payload;

import javax.validation.constraints.*;

public class SignUpRequest {

	@NotBlank
    @Size(min = 3, max = 20)
    private String ime;
	
	@NotBlank
    @Size(min = 3, max = 40)
    private String prezime;

    @NotBlank
    @Size(min = 3, max = 15)
    private String korisnickoIme;

    @NotBlank
    @Size(max = 40)
    @Email
    private String email;
    
    private String mobitel;

    @NotBlank
    @Size(min = 6, max = 100)
    private String lozinka;

    
	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobitel() {
		return mobitel;
	}
	
	public void setMobitel(String mobitel) {
		this.mobitel = mobitel;
	}
	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
    
    


}
