package fer.gamenight.payload;

import java.time.Instant;

public class KorisnikProfile {
	
	private int idKor;
    private String korisnickoIme;
    private Instant joinedAt;

    public KorisnikProfile(int idKor, String korisnickoIme, Instant joinedAt) {
        this.idKor = idKor;
        this.korisnickoIme = korisnickoIme;
        this.joinedAt = joinedAt;
    }
    
    public int getIdKor() {
        return idKor;
    }

    public void setId(int idKor) {
        this.idKor = idKor;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public Instant getJoinedAt() {
        return joinedAt;
    }

    public void setJoinedAt(Instant joinedAt) {
        this.joinedAt = joinedAt;
    }

}
