package fer.gamenight.payload;

public class KorisnikSummary {

	private int idKor;
	private String korisnickoIme;
	
	
	public KorisnikSummary(int idKor, String korisnickoIme) {
		 this.idKor = idKor;
		 this.korisnickoIme = korisnickoIme;
	}
	
	  public int getId() {
	        return idKor;
	    }

	    public void setId(int idKor) {
	        this.idKor = idKor;
	    }

	    public String getKorisnickoIme() {
	        return korisnickoIme;
	    }

	    public void setKorisnickoIme(String korisnickoIme) {
	        this.korisnickoIme = korisnickoIme;
	    }

}
