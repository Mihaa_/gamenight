package fer.gamenight.payload;

public class KorisnikIdentityAvailability {

	private Boolean available;

    public KorisnikIdentityAvailability(Boolean available) {
        this.available = available;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

}
