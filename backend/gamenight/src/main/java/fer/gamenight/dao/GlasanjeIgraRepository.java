package fer.gamenight.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fer.gamenight.domain.GlasanjeDan;
import fer.gamenight.domain.GlasanjeIgra;
import fer.gamenight.domain.GlasanjeIgraKey;

public interface GlasanjeIgraRepository extends JpaRepository<GlasanjeIgra, GlasanjeIgraKey> {
	//public Optional<GlasanjeDan> findByIdKorAndIdDog(GlasanjeIgraKey kljuc);
	public List<GlasanjeIgra> findAllByIdIgra(int idIgra);
}
