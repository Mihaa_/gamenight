package fer.gamenight.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import fer.gamenight.domain.Dogadjaj;

@Repository
public interface DogadjajRepository extends JpaRepository<Dogadjaj, Integer> {

	public Optional<Dogadjaj> findByIdDog(int idDog);

	public Optional<Dogadjaj> findByNaziv(String naziv);
	
}
