package fer.gamenight.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fer.gamenight.domain.GlasanjeDan;
import fer.gamenight.domain.GlasanjeDanKey;
import fer.gamenight.domain.KorDogKey;

public interface GlasanjeDanRepository extends JpaRepository<GlasanjeDan,GlasanjeDanKey> {
	public  Optional<GlasanjeDan> findByIdKorAndIdDog(int idKor, int idDog);
}
