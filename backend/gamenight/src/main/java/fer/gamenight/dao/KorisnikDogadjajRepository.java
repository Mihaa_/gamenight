package fer.gamenight.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fer.gamenight.domain.KorDogKey;
import fer.gamenight.domain.KorisnikDogadjaj;
@Repository
public interface KorisnikDogadjajRepository extends JpaRepository<KorisnikDogadjaj, KorDogKey>{
	
}
