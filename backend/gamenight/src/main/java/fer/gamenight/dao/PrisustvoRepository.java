package fer.gamenight.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fer.gamenight.domain.KorDogKey;
import fer.gamenight.domain.Prisustvo;

public interface PrisustvoRepository extends JpaRepository<Prisustvo, KorDogKey> {
	
}
