package fer.gamenight.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fer.gamenight.domain.UdaljeniKorisnik;

public interface UdaljeniKorisnikRepository extends JpaRepository<UdaljeniKorisnik,Integer> {

}
