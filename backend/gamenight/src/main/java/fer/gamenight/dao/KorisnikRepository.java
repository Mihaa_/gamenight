package fer.gamenight.dao;

import fer.gamenight.domain.*;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KorisnikRepository extends JpaRepository<Korisnik, Integer>{
	
	int countByIdKor(int id);
	
	int countByKorisnickoIme(String korIme);
	
	Optional<Korisnik> findByEmail(String email);
	
	Optional<Korisnik> findByIdKor(int idKor);

    Optional<Korisnik> findByKorisnickoImeOrEmail(String korIme, String email);
	
	Optional<Korisnik> findByKorisnickoIme(String korIme);
	
	Boolean existsByKorisnickoIme(String korIme);

    Boolean existsByEmail(String email);
    
}
