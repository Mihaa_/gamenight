package fer.gamenight.dao;
import fer.gamenight.domain.*;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IgraRepository extends JpaRepository<Igra, Integer>{
	
	int countByNaziv(String naziv);
	
	int countByIdIgre(int id);
	
	Optional<Igra> findByIdIgre(int id);
	
}
