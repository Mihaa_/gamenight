package fer.gamenight.dao;
import fer.gamenight.domain.Role;
import fer.gamenight.domain.RoleName;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer>{

	Optional<Role> findByName(RoleName roleName);
}
