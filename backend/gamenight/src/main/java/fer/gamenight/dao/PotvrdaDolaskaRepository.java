package fer.gamenight.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fer.gamenight.domain.KorDogKey;
import fer.gamenight.domain.PotvrdaDolaska;
import org.springframework.data.jpa.repository.Query;

public interface PotvrdaDolaskaRepository extends JpaRepository<PotvrdaDolaska,KorDogKey> {

}
