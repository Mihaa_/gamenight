package fer.gamenight.domain;

import fer.gamenight.domain.audit.UserDateAudit;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;

@Entity
@Table(name = "dogadjaj")
public class Dogadjaj extends UserDateAudit{


	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idDog;
	
	@NotBlank
    @Size(max = 140)
    private String opis;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinTable(name = "dogadjaj_igra",
    joinColumns = @JoinColumn(name = "idDog"),
    inverseJoinColumns = @JoinColumn(name = "idIgre"))
	private Igra igra;

	@NotNull
	private Instant datumIsteka;
	@NotNull
	private String naziv;

	private boolean izglasan;
	private boolean zavrsen;
	
	public boolean isIzglasan() {
		return izglasan;
	}

	public void setIzglasan(boolean izglasan) {
		this.izglasan = izglasan;
	}

	public boolean isZavrsen() {
		return zavrsen;
	}

	public void setZavrsen(boolean zavrsen) {
		this.zavrsen = zavrsen;
	}

	@NotNull
	private boolean privatni;
	
	public boolean isPrivatni() {
		return privatni;
	}

	public void setPrivatni(boolean privatni) {
		this.privatni = privatni;
	}

	public int getIdDog() {
		return idDog;
	}

	public void setIdDog(int idDog) {
		this.idDog = idDog;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Igra getIgra() {
		return igra;
	}

	public void setIgra(Igra igra) {
		this.igra = igra;
	}

	public Instant getDatumIsteka() {
		return datumIsteka;
	}

	public void setDatumIsteka(Instant datumIsteka) {
		this.datumIsteka = datumIsteka;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
}
