package fer.gamenight.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="korisnikDogadaj") //POPRAVITI ID
@IdClass(KorDogKey.class)
public class KorisnikDogadjaj {

	public KorisnikDogadjaj() {
	}

	@Id
	private int idKor;
	
	@Id
	private int idDog;
	
	public int getIdKor() {
		return idKor;
	}

	public void setIdKor(int idKor) {
		this.idKor = idKor;
	}

	public int getIdDog() {
		return idDog;
	}

	public void setIdDog(int idDog) {
		this.idDog = idDog;
	}

	public KorisnikDogadjaj(int idKor, int idDog) {
		super();
		this.idKor = idKor;
		this.idDog = idDog;
	}
	
	
	
}
