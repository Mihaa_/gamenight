package fer.gamenight.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Igra {

    @Id
    @GeneratedValue
    private int idIgre;
    
    @Column(unique=true)
    @NotNull
    private String naziv;
    
    @Size(max = 140)
    private String opis;
    
    private String link;

    public int getIdIgre() {
        return this.idIgre;
    }

    public void setIdIgre(int idIgre) {
        this.idIgre = idIgre;
    }

    public String getNaziv() {
        return this.naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return this.opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Igra idIgre(int idIgre) {
        this.idIgre = idIgre;
        return this;
    }

    public Igra naziv(String naziv) {
        this.naziv = naziv;
        return this;
    }

    public Igra opis(String opis) {
        this.opis = opis;
        return this;
    }

    public Igra link(String link) {
        this.link = link;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Igra)) {
            return false;
        }
        Igra igra = (Igra) o;
        return idIgre == igra.idIgre && this.naziv.equals(igra.naziv) && this.opis.equals(igra.opis) && this.link.equals(igra.link);
    }

    @Override
    public String toString() {
        return "{" +
            " idIgre='" + getIdIgre() + "'" +
            ", naziv='" + getNaziv() + "'" +
            ", opis='" + getOpis() + "'" +
            ", link='" + getLink() + "'" +
            "}";
    }
    


}