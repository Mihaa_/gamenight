package fer.gamenight.domain;

import java.io.Serializable;

public class KorDogKey implements Serializable {
	private int idDog;
	private int idKor;
	
	public int getIdDog() {
		return idDog;
	}
	public void setIdDog(int idDog) {
		this.idDog = idDog;
	}
	public int getIdKor() {
		return idKor;
	}
	public void setIdKor(int idKor) {
		this.idKor = idKor;
	}
	public KorDogKey(int idDog, int idKor) {
		super();
		this.idDog = idDog;
		this.idKor = idKor;
	}
	public KorDogKey() {
		super();
	}
	
	
}
