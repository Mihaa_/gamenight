package fer.gamenight.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="glasanjeIgra") //POPRAVITI ID
@IdClass(GlasanjeIgraKey.class)
public class GlasanjeIgra {
	
	
	
	public GlasanjeIgra() {
		super();
	}

	@Id
	private int idDog;
	@Id
	private int idKor;
	@Id
	private int	idIgra;
	private double tezinaGlasa;
	
	
	public int getIdDog() {
		return idDog;
	}
	public void setIdDog(int idDog) {
		this.idDog = idDog;
	}
	public int getIdKor() {
		return idKor;
	}
	public void setIdKor(int idKor) {
		this.idKor = idKor;
	}
	public int getIdIgra() {
		return idIgra;
	}
	public void setIdIgra(int idIgra) {
		this.idIgra = idIgra;
	}
	public double getTezinaGlasa() {
		return tezinaGlasa;
	}
	public void setTezinaGlasa(double d) {
		this.tezinaGlasa = d;
	}
	
	public GlasanjeIgra(int idDog, int idKor, int idIgra, double tezinaGlasa) {
		super();
		this.idDog = idDog;
		this.idKor = idKor;
		this.idIgra = idIgra;
		this.tezinaGlasa = tezinaGlasa;
	}
	
}
