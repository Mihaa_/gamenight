package fer.gamenight.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;

public class GlasanjeDanKey implements Serializable {

	public GlasanjeDanKey() {
		super();
	}

	private int idDog;
	private int idKor;
	private Instant dan;
	public int getIdDog() {
		return idDog;
	}
	public void setIdDog(int idDog) {
		this.idDog = idDog;
	}
	public int getIdKor() {
		return idKor;
	}
	public void setIdKor(int idKor) {
		this.idKor = idKor;
	}
	public Instant getDan() {
		return dan;
	}
	public void setDan(Instant date) {
		this.dan = date;
	}
	public GlasanjeDanKey(int idDog, int idKor, Instant date) {
		super();
		this.idDog = idDog;
		this.idKor = idKor;
		this.dan = date;
	}
	
	
}
