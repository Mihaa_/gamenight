package fer.gamenight.domain;


import java.time.Instant;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
@Entity
@Table(name="GlasanjeDan") //POPRAVITI ID
@IdClass(GlasanjeDanKey.class)
public class GlasanjeDan {
	@Id
	int idDog;	
	@Id
	int idKor;
	@Id
	Instant dan;
	
	double tezinaGlasa;
	
	public int getIdDog() {
		return idDog;
	}

	public void setIdDog(int id_dog) {
		this.idDog = id_dog;
	}

	public int getIdKor() {
		return idKor;
	}

	public void setIdKor(int id_kor) {
		this.idKor = id_kor;
	}

	public double getTezinaGlasa() {
		return tezinaGlasa;
	}

	public void setTezinaGlasa(double d) {
		this.tezinaGlasa = d;
	}

	public Instant getDan() {
		return dan;
	}

	public void setDan(Instant dan) {
		this.dan = dan;
	}

	
	
	public GlasanjeDan() {
		super();
	}

	public GlasanjeDan(int idDog, int idKor, Instant dan, double tezina_glasa) {
		super();
		this.idDog = idDog;
		this.idKor = idKor;
		this.dan = dan;
		this.tezinaGlasa = tezina_glasa;
	}
	
	
	
}
