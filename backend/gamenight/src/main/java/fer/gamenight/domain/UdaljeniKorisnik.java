package fer.gamenight.domain;

import java.time.Instant;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class UdaljeniKorisnik {
	
	@Id
	int idKor;
	Instant vrijemeKazne;

	public UdaljeniKorisnik() {
		super();
	}

	public int getIdKor() {
		return idKor;
	}

	public void setIdKor(int idKor) {
		this.idKor = idKor;
	}

	public Instant getVrijemeKazne() {
		return vrijemeKazne;
	}

	public void setVrijemeKazne(Instant vrijemeKazne) {
		this.vrijemeKazne = vrijemeKazne;
	}

	public UdaljeniKorisnik(int idKor, Instant vrijemeKazne) {
		super();
		this.idKor = idKor;
		this.vrijemeKazne = vrijemeKazne;
	}
	
	
}
