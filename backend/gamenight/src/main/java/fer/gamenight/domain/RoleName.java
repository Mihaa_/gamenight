package fer.gamenight.domain;

public enum RoleName {

	ROLE_USER,
    ROLE_ADMIN
}
