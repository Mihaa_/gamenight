package fer.gamenight.domain;

import java.io.Serializable;

public class GlasanjeIgraKey implements Serializable {
	private int idKor;
	private int idDog;
	private int idIgra;
	
	
	
	public GlasanjeIgraKey() {
		super();
	}
	
	public int getIdKor() {
		return idKor;
	}
	public void setIdKor(int idKor) {
		this.idKor = idKor;
	}
	public int getIdDog() {
		return idDog;
	}
	public void setIdDog(int idDog) {
		this.idDog = idDog;
	}

	
	public int getIdIgra() {
		return idIgra;
	}
	public void setIdIgra(int idIgra) {
		this.idIgra = idIgra;
	}
	
	public GlasanjeIgraKey(int idKor, int idDog, int idIgra) {
		super();
		this.idKor = idKor;
		this.idDog = idDog;
		this.idIgra = idIgra;
	}
	
	
}
