package fer.gamenight.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="prisustvo") //POPRAVITI ID
@IdClass(KorDogKey.class)
public class Prisustvo {
	
	@Id
	private int idKor;
	@Id
	private int idDog;
	
	
	public int getIdKor() {
		return idKor;
	}
	public void setIdKor(int idKor) {
		this.idKor = idKor;
	}
	public int getIdDog() {
		return idDog;
	}
	public void setIdDog(int idDog) {
		this.idDog = idDog;
	}
	
	public Prisustvo() {
		
	}
	public Prisustvo(int idKor, int idDog) {
		super();
		this.idKor = idKor;
		this.idDog = idDog;
	}
	
}
