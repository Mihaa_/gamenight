package fer.gamenight.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.SafeHtml.Attribute;

import fer.gamenight.domain.audit.DateAudit;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "korisnik")
public class Korisnik extends DateAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idKor;
	
	@NotBlank
	@Size(max = 20)
	private String ime;
	
	@NotBlank
	@Size(max = 30)
	private String prezime;
	
	@NaturalId
    @NotBlank
    @Size(max = 40)
    @Email
	private String email;
	
	@Min(-100)
	@Max(100)
	private double karma;
	
	public double getKarma() {
		return karma;
	}

	public void setKarma(double karma) {
		this.karma = karma;
	}

	private String mobitel;
	
	@NotBlank
    @Size(max = 100)
	private String lozinka;
	
	@NotBlank
    @Size(max = 15)
	@Column(unique=true)
	@NotNull
	private String korisnickoIme;
	
	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "korisnik_uloge",
            joinColumns = @JoinColumn(name = "korisnik_idKor"),
            inverseJoinColumns = @JoinColumn(name = "uloge_idUloga"))
    private Set<Role> roles = new HashSet<>();
	
	public Korisnik() {

    }

    public Korisnik(String ime, String prezime, String korisnickoIme, String email, String mobitel, String lozinka) {
        this.ime = ime;
        this.prezime = prezime;
        this.korisnickoIme = korisnickoIme;
        this.email = email;
        this.mobitel = mobitel;
        this.lozinka = lozinka;
    }

	public int getIdKor() {
		return idKor;
	}

	public void setIdKor(int idKor) {
		this.idKor = idKor;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobitel() {
		return mobitel;
	}

	public void setMobitel(String mobitel) {
		this.mobitel = mobitel;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}
	
	public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

	@Override
	public String toString() {
		return "{" +
				" IdKor=" + this.getIdKor() + "'"+
	            ", Ime=" + this.getIme() + "'" +
	            ", Prezime='" + this.getPrezime() + "'" +
	            ", E-mail='" + this.getEmail() + "'" +
	            ", Mobitel='" + this.getMobitel() + "'" +
	            ", Korisnicko ime=" + this.getKorisnickoIme() + "'" +
	            ", Lozinka= " + this.getLozinka() + "'" +
	            "}";
	}
	
	
}
