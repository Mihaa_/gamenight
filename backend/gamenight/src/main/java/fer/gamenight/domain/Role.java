package fer.gamenight.domain;

import javax.persistence.*;

import org.hibernate.annotations.NaturalId;

@Entity
@Table(name = "uloge")
public class Role {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idUloga;
	
	@Enumerated(EnumType.STRING)
    @NaturalId
    @Column(length = 60)
    private RoleName name;

	public Role() {

    }

    public Role(RoleName name) {
        this.name = name;
    }
    
    public int getId() {
        return idUloga;
    }

    public void setId(int id) {
        this.idUloga = id;
    }

    public RoleName getName() {
        return name;
    }

    public void setName(RoleName name) {
        this.name = name;
    }

}
